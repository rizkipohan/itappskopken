<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemName extends Model
{
    use SoftDeletes;

    protected $table = 'mst_item_name';

    protected $guarded = [];

    public function itemtype()
    {
        return $this->belongsTo('App\ItemType','item_type_id');
    }

    public function itemdetail()
    {
        return $this->hasMany('App\ItemDetail');
    }
}
