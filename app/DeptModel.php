<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeptModel extends Model
{
    use SoftDeletes;

    protected $table = 'mst_dept';

    protected $guarded = [];
}
