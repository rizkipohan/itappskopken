<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class SuperAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('superadmin');
    }

   
    public function indexUser()
    {
        $users = User::all();
        // dd($users);
        return view('user.index')->with('users',$users);
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

   
    public function show(User $user)
    {
        //
    }

    
    public function editUser(User $user)
    {
        $roles = Role::all();

        return view('user.edit')->with(['user'=>$user,'roles'=>$roles]);
    }


    public function updateUser(Request $request, User $user)
    {
        User::where('id',$user->id)->update(['role_id' => $request->role]);
        return redirect('superadmin/user/index');
    }

    
    public function destroy(User $user)
    {
        //
    }
}
