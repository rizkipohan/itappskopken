<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StoreProperties;
use App\Store;
use App\Mail\StoreManagerApproval;
use App\Mail\NsoTeamNotification;
use App\Mail\StorePropertiesReport;
use Illuminate\Support\Facades\Schema;
use Mail;
use Auth;
date_default_timezone_set('Asia/Jakarta');


class StorePropertiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getStores(){
        return Store::all();
    }

    public function create(){
        $columns = Schema::getColumnListing('store_properties');
        // dd($columns);
        return view('storeproperties.create',['stores'=>$this->getStores(),'columns'=>$columns]);
    }

    public function store(Request $request){

        $columns = Schema::getColumnListing('store_properties');

        $reqs = (object) $request->all();
        // dd($reqs);

        $props = StoreProperties::create([
            'store_id' => $request->store_id,
            'open_date' => $request->open_date,
            'pos' => $request->pos,
            'pos_wifi' => $request->pos_wifi,
            'wifi_extended_cable' => $request->wifi_extended_cable,
            'data_product_promo' => $request->data_product_promo,
            'employee_register' => $request->employee_register,
            'employee_training' => $request->employee_training,
            'pos_trainer' => $request->pos_trainer,
            'printer_label' => $request->printer_label,
            'usb_lcd_content' => $request->usb_lcd_content,
            'tablet' => $request->tablet,
            'apps_id' => $request->apps_id,
            'apps_pin' => $request->apps_pin,
            't_hub' => $request->t_hub,
            'email' => $request->email,
            'laptop' => $request->laptop,
            'internet' => $request->internet,
            'isp' => $request->isp,
            'sid' => $request->sid,
            'ip_public' => $request->ip_public,
            'ip_reserved' => $request->ip_reserved,
            'cctv_cam_qty' => $request->cctv_cam_qty,
            'cctv_recording' => $request->cctv_recording,
            'cctv_cloud_id' => $request->cctv_cloud_id,
            'cctv_streaming' => $request->cctv_streaming,
            'cctv_regional' => $request->cctv_regional,
            'code_creating_order' => $request->code_creating_order,
            'create_offline_order' => $request->create_offline_order,
            'check_order_appear_in_apps' => $request->check_order_appear_in_apps,
            'scan_qr_code_create_order' => $request->scan_qr_code_create_order,
            'pic_name' => $request->pic_name,
            'pic_phone' => $request->pic_phone,
            'pic_email' => $request->pic_email,
            'created_by' => Auth::user()->id
        ]);

        // dd($props->store);
        Mail::to($request->pic_email)->send(new StoreManagerApproval($reqs,$props,$columns));
        Mail::to('nso@kopikenangan.com')->send(new NsoTeamNotification($reqs,$props,$columns));

        return redirect('storeprops/create')->with('success','berhasil submit!');
    }

    public function storePropReport()
    {
        $datenow = date("Y-m-d");
        $backdate = date('Y-m-d', strtotime($datenow."-32 days"));
        // dd($datenow,$backdate);

        $data = StoreProperties::where('open_date','>=',$backdate)
        ->where('open_date','<=',$datenow)
        ->orderBy('open_date')
        ->get();

        // Mail::to('nso@kopikenangan.com')->send(new StorePropertiesReport($data));
        Mail::to('rizki.pohan@kopikenangan.com')->send(new StorePropertiesReport($data));
        
    }
}
