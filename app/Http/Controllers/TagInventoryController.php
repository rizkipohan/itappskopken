<?php

namespace App\Http\Controllers;

use App\TagInventory;
use App\Item;
use App\Store;
use Auth;
use Illuminate\Http\Request;
use Datatables;


class TagInventoryController extends Controller
{
    /*
        ---------- status barang ----------
        0 -> available
        1 -> deployed to store
        2 -> to service
        3 -> return from service
    */

    public function items()
    {
        // $a = Item::where('department_id',Auth::user()->department_id)->where('show','yes')->get();
        $a = Item::where('department_id',Auth::user()->department_id)->get();
        return $a;
    }

    public function tagInventory()
    {
        $all = TagInventory::all();
        return $all;
    }
    
    public function stores()
    {
        $a = Store::all();
        return $a;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('taginventory.index',['tags'=>$this->tagInventory()]);
    }

    public function indexTable()
    {
        return Datatables::of(TagInventory::all())->make(true);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('taginventory.create',['items'=>$this->items()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        TagInventory::create([
            'item_id'=>$request->item_id, 
            'serial_number'=>$request->serial_number, 
            'status'=>$request->status, 
            'received_at'=>$request->received_at, 
            'deployed_at'=>$request->deployed_at, 
            'service_at'=>$request->service_at, 
            'store_id'=>$request->store_id
        ]);
        return redirect('taginventory/index')->with(['success'=>'berhasil tambah data!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TagInventory  $tagInventory
     * @return \Illuminate\Http\Response
     */
    public function show(TagInventory $tagInventory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TagInventory  $tagInventory
     * @return \Illuminate\Http\Response
     */
    public function edit(TagInventory $tagInventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TagInventory  $tagInventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TagInventory $tagInventory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TagInventory  $tagInventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(TagInventory $tagInventory)
    {
        //
    }

    public function deployCreate(Request $request,$id)
    {   
        $tags = TagInventory::findOrFail($id);

        return view('taginventory.deployCreate',[
            'tags'=>$tags,
            'stores'=>$this->stores()
        ]);
    }

    public function deployUpdate(Request $request,$id)
    {
        $tags = TagInventory::findOrFail($id)->update([
            // 'item_id'=>$request->item_id, 
            // 'serial_number'=>$request->serial_number, 
            'status'=>'1', 
            // 'received_at'=>$request->received_at, 
            'deployed_at'=>$request->deployed_at, 
            // 'service_at'=>$request->service_at, 
            'store_id'=>$request->store_id
        ]);

        return redirect('taginventory/index');
    }

}
