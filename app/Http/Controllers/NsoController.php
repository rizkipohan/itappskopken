<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Store;
use App\Nso;

class NsoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $nso = Nso::paginate(25);
        return view('nso.index',['nso' => $nso]);
    }

    public function create()
    {
        $stores = Store::all();
        return view('nso.create',compact('stores'));
    }

    public function store(Request $request)
    {
        $nso = $request->all();
        // dd($request->all());

        if($request->file('layout')!=NUll)
        {
            $this->validate($request, [
                'layout' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
    
            $files = $request->file('layout');
            $destinationPath = public_path('layout/'); 
            $imgName = date('YmdHis').'.'.$files->extension();
            $files->move($destinationPath, $imgName);
            // dd($files);
            $img = url('public/layout').'/'.$imgName;
            $nso['img'] = $img;
            $nso['img_name'] = $imgName;
        }else{
            $img=null;
        }

        $json = json_encode($nso);
        // dd($json);
        
        Nso::create([
            'store_id' => $request->store_id,
            'json' => $json
        ]);
        // dd($json);
        return redirect('nso/index');
    }

    public function edit($id)
    {
        $nso = Nso::where('id','=',$id)->first();
        $stores = Store::all();
        $item = (object) json_decode($nso->json);
        // dd($nso);
        return view('nso.edit',compact('nso','stores','item'));
    }

    public function update(Request $request,$id)
    {
        $nso = $request->all();
        // dd($request->all());

        if($request->file('layout_edit')!=NUll)
        {
            $this->validate($request, [
                'layout' => 'image|mimes:jpeg,png,jpg,gif,svg|max:16384',
            ]);
    
            $files = $request->file('layout_edit');
            $destinationPath = public_path('layout/'); 
            $imgName = date('YmdHis').'.'.$files->extension();
            $files->move($destinationPath, $imgName);
            // dd($files);
            $img = url('public/layout').'/'.$imgName;
            $nso['img'] = $img;
            $nso['img_name'] = $imgName;
        }else{
            
            if(isset($request->img)){
                $nso['img'] = $request->img;
            }
            
            if(isset($request->img_name)){
                $nso['img_name'] = $request->img_name;
            }
        }

        $json = json_encode($nso);
        // dd($json);
        
        Nso::where('id',$id)
        ->update([
            'store_id' => $request->store_id,
            'json' => $json
        ]);
        // dd($json);
        return redirect('nso/index');
    }

    // Print preview ====================================================
    public function pdf(Request $request,$id)
    {
        $nso = Nso::where('id',$id)->first();
        $item = (object) json_decode($nso->json);
        $stores = Store::where('id',$item->store_id)->first();
        // dd($nso);
        return view('nso.print',compact('nso','stores','item'));
        
    }

    // Print the PDF ===================================================
    public function download(Request $request)
    {
        $stores = (object) $request->all();

        if($request->file('layout')!=NUll)
        {
            $this->validate($request, [
                'layout' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
    
            $files = $request->file('layout');
            $destinationPath = public_path('layout/'); 
            $imgName = date('YmdHis').'.'.$files->extension();
            // $files->move($destinationPath, $imgName);
            $img = url('public/layout').'/'.$imgName;
            $nso['img'] = $img;
            $nso['img_name'] = $imgName;
        }else{
            $img=null;
        }

        $pdf = PDF::loadview('nso.pdf.nso',['stores'=>$stores,'img'=>$img]);
        return 
        $pdf->download('NSO-'.$stores->store_name.'-'.date('Ymd').'.pdf');
        // $pdf->stream();
    }

}
