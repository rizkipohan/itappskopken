<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use App\Store;
use App\ItemType;
use App\ItemName;
use App\VendorModel;
use App\ItemDetail;
use App\Trxitemin;
use App\ClassModel;
use App\DeptModel;
use App\Designation;
use App\Trxitemout;

class TemporaryController extends Controller
{
    public function cekfamcode(){
        $items = ItemDetail::whereNotNull('fam_code')
        ->where('fam_code','!=','NA')->get();
        $itemsUnique = $items->unique('fam_code');
        $itemsDupes = $items->diff($itemsUnique);
        
        return view('itasset.cekfamcode',['items'=>$itemsDupes]);
    }
}
