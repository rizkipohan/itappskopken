<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagInventory extends Model
{
    //
    protected $fillable = [
        'name', 'serial_number', 'status', 'received_at', 'deployed_at', 'service_at', 'store_id'
    ];

    public function store()
    {
        return $this->belongsTo('App\Store');
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }
}
