<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';
    
    protected $fillable = [
        'id','department_name'
    ];

    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function ticket()
    {
        return $this->hasMany('Kordy\Ticketit\Models\Ticket', 'department_id');
    }
}
