<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemOut extends Model
{
    protected $table = 'item_out';

    protected $fillable = [
        'item_id','store_id','qty','serial_number','notes','date','status','reason','employee_name','request_item_id'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
}
