<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorModel extends Model
{
    use SoftDeletes;

    protected $table = 'mst_vendor';

    protected $guarded = [];

    // public function itemname()
    // {
    //     return $this->hasMany('App\ItemName');
    // }
}
