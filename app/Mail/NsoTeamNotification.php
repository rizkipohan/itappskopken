<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NsoTeamNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $reqs,$props,$columns;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reqs,$props,$columns)
    {
        $this->reqs = $reqs;
        $this->props = $props;
        $this->columns = $columns;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.NsoTeamNotification');
    }
}
