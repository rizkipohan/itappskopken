<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = [
        'name', 'email', 'phone',
    ];

    public function store()
    {
        return $this->hasMany('App\Store');
    }
}
