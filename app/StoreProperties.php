<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreProperties extends Model
{
    use SoftDeletes;

    protected $table = 'store_properties';

    protected $guarded = [];

    public function store()
    {
        return $this->belongsTo('App\Store');
    }

    public function created_by_user()
    {
        return $this->belongsTo('App\User','created_by');
    }
}
