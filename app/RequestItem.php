<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestItem extends Model
{
    protected $table = 'request_item';

    // protected $fillable = [
    //     'store_id','items','created_by','approve_status'
    // ];

    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
}
