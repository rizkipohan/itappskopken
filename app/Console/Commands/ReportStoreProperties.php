<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\StoreProperties;
use App\Mail\StorePropertiesReport;
use Mail;


class ReportStoreProperties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Report Store Properties';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $datenow = date("Y-m-d");
        $backdate = date('Y-m-d', strtotime($datenow."-31 days"));

        $data = StoreProperties::where('created_at','>=',$backdate)
        ->where('open_date','<=',$datenow)
        ->orderBy('open_date')
        ->get();

        // Mail::to($request->pic_email)->send(new StoreManagerApproval($reqs,$props,$columns));
        Mail::to('nso@kopikenangan.com')->send(new StorePropertiesReport($data));
    }
}
