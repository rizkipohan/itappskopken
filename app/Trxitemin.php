<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trxitemin extends Model
{
    use SoftDeletes;

    protected $table = 'trx_item_ins';

    protected $guarded = [];

    public function itemname()
    {
        return $this->belongsTo('App\ItemName','item_name_id');
    }

    public function itemtype()
    {
        return $this->belongsTo('App\ItemType','item_type_id');
    }

    public function itemdetail()
    {
        return $this->belongsTo('App\ItemDetail','item_detail_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Store','from_location');
    }

    public function vendor()
    {
        return $this->belongsTo('App\VendorModel','vendor_id');
    }

    public function createdby()
    {
        return $this->belongsTo('App\User','created_by');
    }
}
