<?php

namespace App\Exports;

use App\Item;
use App\ItemIn;
use App\ItemOut;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class ItemDetailExport implements FromView
{
    use Exportable;

    public function __construct(int $item)
    {
        $this->item = $item;
        return $this->item;
    }

    public function view(): View
    {
        $item = $this->item;
        // $x = Item::query()->with([
        //     'itemin' => function($query) use ($item)
        //                 {
        //                     $query->where('item_id',$item); 
        //                 }, 
        //     'itemout' => function($query) use ($item)
        //                 {
        //                     $query->where('item_id',$item); 
        //                 }
        // ])->get();
        $itemin = ItemIn::where('item_id',$item)->get();
        $itemout = ItemOut::where('item_id',$item)->get();
        // dd($itemin);
        return view('inventory.report.itemdetail.itemdetailexport',['itemin'=>$itemin,'itemout'=>$itemout]);
    }
}


