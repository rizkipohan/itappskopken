<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemIn extends Model
{
    protected $table = 'item_in';

    protected $fillable = [
        'item_id','qty','from','store_id','vendor','notes','date','po_number','pr_number'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
}
