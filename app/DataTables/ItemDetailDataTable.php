<?php

namespace App\DataTables;

use App\ItemDetail;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ItemDetailDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $model = ItemDetail::with(['itemname','itemtype','vendor']);

        return DataTables::of($model)
        ->addIndexColumn()
        ->addColumn('itemname', function (ItemDetail $trx) {
            return $trx->itemname->name;
        })
        ->addColumn('itemtype', function (ItemDetail $trx) {
            return $trx->itemtype->name;
        })
        ->addColumn('vendor', function (ItemDetail $trx) {
            return $trx->vendor->name;
        })
        ->addColumn('asset_code', function (ItemDetail $trx) {
            return $trx->it_asset_code.$trx->increment_id;
        })
        ->toJson();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\App\ItemDetail $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ItemDetailDataTable $data)
    {
       
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->dom('Bfrtip')
        ->orderBy(1)
        ->buttons(
            Button::make('excel')
        );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ItemDetail_' . date('YmdHis');
    }
}
