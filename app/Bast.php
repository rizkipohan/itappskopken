<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bast extends Model
{
    protected $table = 'bast_nso';
    
    protected $fillable  = [
        'store_id','path','filename'
    ];

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
    
}
