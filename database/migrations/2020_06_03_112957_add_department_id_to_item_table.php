<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDepartmentIdToItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->BigInteger('department_id')->unsigned()->nullable();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        });
        Schema::table('item_in', function (Blueprint $table) {
            $table->BigInteger('department_id')->unsigned()->nullable();
            // $table->string('warehouse')->nullable();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        });
        Schema::table('item_out', function (Blueprint $table) {
            $table->BigInteger('department_id')->unsigned()->nullable();
            $table->string('employee_name')->nullable();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item', function (Blueprint $table) {
            //
        });
    }
}
