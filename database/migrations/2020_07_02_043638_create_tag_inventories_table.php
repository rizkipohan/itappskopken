<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('item_id')->unsigned()->nullable();
            $table->string('serial_number')->unique()->nullable();
            $table->enum('status',[0,1,2,3])->default(0);
            $table->date('received_at')->nullable();
            $table->date('deployed_at')->nullable();
            $table->date('service_at')->nullable();
            $table->date('return_at')->nullable();
            $table->BigInteger('store_id')->unsigned()->nullable();

            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_inventories');
    }
}
