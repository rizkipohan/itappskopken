<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->string('store_name');
            $table->string('store_code')->unique();
            $table->string('store_status')->nullable();
            $table->date('open_date')->nullable();
            $table->longText('store_address')->nullable();
            $table->string('store_email')->nullable();
            $table->string('store_phone')->nullable();
            $table->string('tax_number')->nullable();
            $table->string('connection_status')->nullable();
            $table->string('provider')->nullable();
            $table->string('sid')->nullable();
            $table->string('mikrotik_status')->nullable();
            $table->string('ip_local')->nullable();
            $table->string('ip_wan')->nullable();
            $table->string('ip_gateway_wan')->nullable();
            $table->string('ip_dns')->nullable();
            $table->string('ip_vpn')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
