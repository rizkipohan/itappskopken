<?php

use App\Http\Controllers\NsoController;
use Illuminate\Support\Facades\Route;
date_default_timezone_set('Asia/Jakarta');
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('/ajaxDept', 'AjaxController@dept');

route::get('/draw', function() {
    return view('signature');
});

Route::get('/cekfamcode', 'TemporaryController@cekfamcode');

Auth::routes();

/*
=======================================================================================================================
============================================= ITASSET NUMBER ==========================================================
=======================================================================================================================
*/
Route::prefix('itasset')->group(function () {

    Route::get('/','ItAssetController@index');

    Route::prefix('master')->group(function () {
        Route::prefix('itemtype')->group(function () {
            Route::get('index','ItAssetController@itemTypeIndex');
            Route::get('delete/{id}','ItAssetController@itemTypeDelete');
            Route::get('edit/{id}','ItAssetController@itemTypeEdit');
            Route::post('update/{id}','ItAssetController@itemTypeUpdate');
        });
        Route::prefix('itemname')->group(function () {
            Route::get('index','ItAssetController@itemNameIndex');
            Route::get('delete/{id}','ItAssetController@itemNameDelete');
            Route::get('edit/{id}','ItAssetController@itemNameEdit');
            Route::post('update/{id}','ItAssetController@itemNameUpdate');
        });
        Route::prefix('vendor')->group(function () {
            Route::get('index','ItAssetController@vendorIndex');
            Route::get('delete/{id}','ItAssetController@vendorDelete');
            Route::get('edit/{id}','ItAssetController@vendorEdit');
            Route::post('update/{id}','ItAssetController@vendorUpdate');
        });
        Route::prefix('class')->group(function () {
            Route::get('index','ItAssetController@classIndex');
            Route::get('delete/{id}','ItAssetController@classDelete');
            Route::get('edit/{id}','ItAssetController@classEdit');
            Route::post('update/{id}','ItAssetController@classUpdate');
        });
        Route::prefix('dept')->group(function () {
            Route::get('index','ItAssetController@deptIndex');
            Route::get('delete/{id}','ItAssetController@deptDelete');
            Route::get('edit/{id}','ItAssetController@deptEdit');
            Route::post('update/{id}','ItAssetController@deptUpdate');
        });
        Route::prefix('designation')->group(function () {
            Route::get('index','ItAssetController@designationIndex');
            Route::get('delete/{id}','ItAssetController@designationDelete');
            Route::get('edit/{id}','ItAssetController@designationEdit');
            Route::post('update/{id}','ItAssetController@designationUpdate');
        });
        Route::prefix('location')->group(function () {
            Route::get('index','ItAssetController@locationIndex');
            Route::get('delete/{id}','ItAssetController@locationDelete');
            Route::get('edit/{id}','ItAssetController@locationEdit');
            Route::post('update/{id}','ItAssetController@locationUpdate');
            Route::get('datalocationindex','ItAssetController@dataLocationIndex');

        });
        // Route::prefix('itemdetail')->group(function () {
        //     Route::get('index','ItAssetController@itemDetailIndex');
        //     Route::get('delete/{id}','ItAssetController@itemDetailDelete');
        // });
    });

    //BUAT JQUERY
    Route::prefix('jq')->group(function () {
        //item type
        Route::get('getitemtype','ItAssetController@getItemTypejq');
        Route::post('additemtype','ItAssetController@addItemTypejq');
        //item name
        Route::get('getitemname','ItAssetController@getItemNamejq');
        Route::post('getitemnamebytype','ItAssetController@getItemNameByTypejq');
        Route::post('additemname','ItAssetController@addItemNamejq');
        //vendor
        Route::get('getvendor','ItAssetController@getVendorjq');
        Route::post('addvendor','ItAssetController@addVendorjq');
        //class
        Route::post('addclass','ItAssetController@addClassjq');
        //dept
        Route::post('adddept','ItAssetController@addDeptjq');
        //designation
        Route::get('getdesignation','ItAssetController@getDesignationjq');
        Route::post('adddesignation','ItAssetController@addDesignationjq');
        //location
        Route::get('getlocation','ItAssetController@getLocationjq')->name('get_location');
        Route::post('addlocation','ItAssetController@addLocationjq');
        // Route::post('/','@');

        Route::prefix('book')->group(function(){
            Route::get('getitembook','ItAssetController@getItemBookjq');
            Route::get('getclassbook','ItAssetController@getClassBookjq');
            Route::get('getdeptbook','ItAssetController@getDeptBookjq');
            Route::get('getdesignationbook','ItAssetController@getDesignationBookjq');
            Route::get('getlocationbook','ItAssetController@getLocationBookjq');

        });

    });

    Route::prefix('itemdetail')->group(function () {
        Route::get('create','ItAssetController@itemDetailCreate');
        Route::post('store','ItAssetController@itemDetailStore');
        Route::get('index','ItAssetController@itemDetailIndex')->name('item_index');
        // Route::get('index/ho','ItAssetController@itemDetailIndexHo')->name('item_index_ho');
        // Route::get('index/avail','ItAssetController@itemDetailIndexAvail')->name('item_index_avail');
        Route::get('dataitemdetail','ItAssetController@dataItemDetail');
        Route::get('dataitemdetailho','ItAssetController@dataItemDetailHo');
        Route::get('dataitemdetailavail','ItAssetController@dataItemDetailAvail');
        Route::get('printlable/{id}','ItAssetController@printLable')->name('print_lable');
        Route::prefix('edit')->group(function(){
            Route::get('famcode/{id}','ItAssetController@editfamcode');
        });
        Route::prefix('update')->group(function(){
            Route::post('famcode/{id}','ItAssetController@updatefamcode');
        });
        Route::get('cancel/booking/{id}','ItAssetController@cancelBooking');
        Route::get('delete/item/{id}','ItAssetController@deleteItem');

        Route::prefix('index')->group(function(){  
            Route::get('ho','ItAssetController@itemDetailIndexHo');
            Route::get('avail','ItAssetController@itemDetailIndexAvail');
            Route::prefix('edit')->group(function(){
                Route::get('famcode/{id}','ItAssetController@editfamcode');
            });
            Route::prefix('update')->group(function(){
                Route::post('famcode/{id}','ItAssetController@updatefamcode');
            });
            Route::get('cancel/booking/{id}','ItAssetController@cancelBooking');
            Route::get('delete/item/{id}','ItAssetController@deleteItem');
        });
    });


    Route::prefix('booking')->group(function () {
        Route::get('create','ItAssetController@bookingCreate');
        Route::post('bookitem','ItAssetController@bookingItem');
    });

    Route::prefix('checkout')->group(function () {
        Route::get('selectlocation','ItAssetController@selectLocation');
        Route::post('showbookeditem','ItAssetController@showBookedItem');
        Route::post('checkoutitem','ItAssetController@checkOutItem')->name('checkout_item');
    });

    Route::prefix('transfer')->group(function () {
        Route::prefix('in')->group(function(){
            Route::get('selectlocation','ItAssetController@trfInSelectLocation');
            Route::post('create','ItAssetController@trfInCreate')->name('create_trfin');
            Route::post('store','ItAssetController@trfInStore')->name('store_trfin');  
        });
        Route::prefix('out')->group(function(){
            Route::get('create','ItAssetController@trfOutCreate')->name('transfer_out');
            Route::post('store','ItAssetController@trfOutStore')->name('store_trfout');
        });
            
    });

    Route::prefix('report')->group(function(){
        Route::prefix('byitem')->group(function(){
            Route::get('index','ItAssetController@reportByItemIndex')->name('report_byitem');
            Route::get('datareportbyitemindex','ItAssetController@dataReportByItemIndex')->name('datareportbyitem');
            Route::get('trxindex/{id}','ItAssetController@reportByItemDetails');
        });
        Route::prefix('bylastlocation')->group(function(){
            Route::get('index','ItAssetController@reportByLastLocationIndex')->name('report_bylastlocation');
            Route::post('lastlocationindex','ItAssetController@reportByLastLocationDetails')->name('databylastlocation');
        });
        Route::prefix('byitemqty')->group(function(){
            Route::get('index','ItAssetController@reportByItemQtyIndex')->name('report_item_qty');
        });

    });

});
/*
==================================================================================================================================================
==================================================================================================================================================
==================================================================================================================================================
*/
Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

// route::namespace('superadmin')->prefix('superadmin')->name('superadmin.')->group(function(){
//     route::resource('/users','UsersController',['except'=>['show','create','store']]);
// });

// Edit User Role (superadmin only)
route::get('/superadmin/user/index','SuperAdminController@indexUser');
route::get('/superadmin/user/edit/{user}','SuperAdminController@editUser');
route::post('/superadmin/user/update/{user}','SuperAdminController@updateUser');

route::get('/user/edit/{user}','EditUserController@edit');
route::post('/user/update/{user}','EditUserController@update');

// IT Documents
Route::prefix('itdocs')->group(function () {
    Route::get('/','ItdocsController@index');
});

// BAST
Route::prefix('bast')->group(function () {
    Route::get('/','BastController@index');
    Route::get('/create','BastController@create');
    Route::post('/store','BastController@store');
});

// Request Item
Route::prefix('requestitem')->group(function () {
    // Route::get('/','RequestItemController@index');
    Route::get('/create','RequestItemController@create');
    Route::post('/store','RequestItemController@store');
    Route::get('/edit/{id}','RequestItemController@edit');
    Route::get('/update/{id}','RequestItemController@update');
});

//
Route::prefix('storeprops')->group(function () {
    // Route::get('/','StorePropertiesController@index');
    Route::get('/create','StorePropertiesController@create');
    Route::post('/store','StorePropertiesController@store');
    Route::get('/storepropreport','StorePropertiesController@storePropReport')->name('StorePropertiesReport');
    // Route::get('/edit/{id}','RequestItemController@edit');
    // Route::get('/update/{id}','RequestItemController@update');
});

// Master Home
route::get('/master','MasterController@index');

// Store Table CRUD
route::get('master/store/index','StoreController@index');
route::get('master/store/create','StoreController@create');
route::post('master/store/storing','StoreController@store');
route::get('master/store/edit/{store}','StoreController@edit');
route::post('master/store/update/{store}','StoreController@update');
route::get('master/store/delete/{store}','StoreController@delete');
route::post('master/store/destroy/{store}','StoreController@destroy');
route::get('master/store/search','StoreController@search');

// Inventory Home
route::get('inventory/','InventoryController@indexHome');

// Inventory CRUD
route::get('inventory/item/index/{dpt}','InventoryController@index');
route::get('inventory/item/create','InventoryController@create');
route::post('inventory/item/store','InventoryController@store');
route::get('inventory/item/edit/{item}','InventoryController@edit');
route::post('inventory/item/update/{item}','InventoryController@update');
route::get('inventory/item/delete/{item}','InventoryController@delete');
route::post('inventory/item/destroy/{item}','InventoryController@destroy');

route::get('inventory/itemout/create','InventoryController@itemOutCreate');
route::post('inventory/itemout/store','InventoryController@itemOutStore');
route::get('inventory/itemout/index/{dpt}','InventoryController@itemOutIndex');
route::get('inventory/itemout/delete/{item}','InventoryController@itemOutDelete');
route::get('inventory/itemout/edit/{item}','InventoryController@itemOutEdit');
route::post('inventory/itemout/update/{item}','InventoryController@itemOutUpdate');
route::get('inventory/itemout/search','InventoryController@itemOutSearch');

route::get('inventory/itemin/create','InventoryController@itemInCreate');
route::post('inventory/itemin/store','InventoryController@itemInStore');
route::get('inventory/itemin/index/{dpt}','InventoryController@itemInIndex');
route::get('inventory/itemin/delete/{id}','InventoryController@itemInDelete');
route::get('inventory/itemin/edit/{item}','InventoryController@itemInEdit');
route::post('inventory/itemin/update/{item}','InventoryController@itemInUpdate');
route::get('inventory/itemin/search','InventoryController@itemInSearch');

route::get('inventory/cek/{item}','InventoryController@cek');

route::get('inventory/report/selectDept','InventoryController@selectDepartment');
route::get('inventory/report/itembalance/index','InventoryController@reportItemBalanceIndex');
route::get('inventory/report/itembalance/export/{dpt}','InventoryController@reportItemBalanceExport');
// route::get('inventory/report/itemdetail/index','InventoryController@reportItemDetailIndex');
route::get('inventory/report/itemdetail/export/{item}','InventoryController@reportItemDetailExport');

// Inventory Tagging
route::get('taginventory/index','TagInventoryController@index');
route::get('taginventory/create','TagInventoryController@create');
route::post('taginventory/store','TagInventoryController@store');
route::get('taginventory/deploy/create/{id}','TagInventoryController@deployCreate');
route::post('taginventory/deploy/update/{id}','TagInventoryController@deployUpdate');

// NSO Approve
route::get('nso/index','NsoController@index');
route::get('nso/create','NsoController@create');
route::post('nso/store','NsoController@store');
route::get('nso/edit/{id}','NsoController@edit');
route::post('nso/update/{id}','NsoController@update');
route::get('nso/print/{id}','NsoController@pdf');
route::post('nso/pdf/download','NsoController@download');
// route::get('/tickets', function(){
//     return redirect()->action('\Kordy\Ticketit\Controllers\TicketsController@index');
// });

// L&D File
route::get('/folder/create','FolderController@create');
route::get('/folder/index','FolderController@index');
route::post('/folder/store','FolderController@store');
route::get('/folder/file/index/{id}','FileController@indexFolder');

route::get('/file/index','FileController@index');
route::get('/file/create','FileController@create');
route::post('/file/upload','FileController@upload');
// route::get('/file/show/{id}','FileController@show');