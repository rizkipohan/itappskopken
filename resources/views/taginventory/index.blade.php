@extends('taginventory.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">List</h1>
        </div>
    </div>

    <div class="row justify-content-center my-3">
        <a name="" id="" class="btn btn-success mx-2" href="{{url('taginventory/create')}}" role="button">Add</a>
    </div>
    
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="table-responsive">
                <table class="table table-striped table-bordered text-center" id="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Item Name</th>
                            <th>Serial Number</th>
                            <th>Status</th>
                            {{-- <th>Received At</th> --}}
                            <th>Deployed At</th>
                            <th>Deployed To</th>
                            {{-- <th>Service At</th> --}}
                            {{-- <th>Return from Service at</th> --}}
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                            @if(count($tags)<=0)
                            <tr><td colspan="100"></td></tr>
                            @else
                                @foreach ($tags as $key => $tag)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$tag->item['name']}}</td>
                                    <td>{{$tag->serial_number}}</td>
                                    <td>{{$tag->status}}</td>
                                    {{-- <td>{{$tag->received_at}}</td> --}}
                                    <td>{{$tag->deployed_at}}</td>
                                    <td>{{$tag->store['store_name']}}</td>
                                    {{-- <td>{{$tag->service_at}}</td> --}}
                                    {{-- <td>{{$tag->return_at}}</td> --}}
                                    <td>
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Dropdown button
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{url('taginventory/deploy/create/'.$tag->id)}}">Checkout/Print Label</a>
                                            {{-- <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a> --}}
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                       
                    </tbody>
                </table>
                <script>
                   $(document).ready(function(){
                        $('#table').DataTable();
                    });
                </script>
            </div>
        </div>
    </div>
</div>
@endsection
