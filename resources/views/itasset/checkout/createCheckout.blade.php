@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Checkout Item</h3>
        </div>
    </div>

    <form action="{{route('checkout_item')}}" method="POST">
        @csrf
        <div class="row justify-content-center">
            <div class="col-md">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Item Type</th>
                                <th>Item Name</th>
                                <th>FAM Code</th>
                                <th>IT Asset Code</th>
                                <th>Serial Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $key=>$item)
                            <tr>
                                <input type="hidden" name="item_detail_id[{{$key}}]" value="{{$item->id}}">
                                <td>{{$key+1}}</td>
                                <td>{{$item->itemtype->name}}</td>
                                <td>{{$item->itemname->name}}</td>
                                <td>{{$item->fam_code}}</td>
                                <td>{{$item->it_asset_code.$item->increment_id}}</td>
                                <td>
                                    <input type="text" class="form-control" name="serial_number[{{$key}}]" 
                                    @if ($item->serial_number != null) 
                                        value="{{$item->serial_number}}" readonly
                                    @endif
                                    >
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @if (count($items)!=null)
        <div class="row justify-content-center">
            <div class="col-md-6 d-grid">
                <button type="submit" class="btn btn-success">Check Out</button>
            </div>
        </div>
        @endif


    </form>
</div>

<script>

</script>

@endsection
