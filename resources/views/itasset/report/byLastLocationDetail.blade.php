@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Item List by Location</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped text-center" id="data-table">
                    <thead>
                        <th>#</th>
                        <th>Item Type</th>
                        <th>Item Name</th>
                        <th>Origin</th>
                        <th>Last Location</th>
                        <th>Asset Code</th>
                        <th>FAM Code</th>
                        <th>S/N</th>
                    </thead>
                    <tbody>
                        @foreach ($items as $key => $item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$item->itemtype->name}}</td>
                                <td>{{$item->itemname->name}}</td>
                                <td>{{$item->location->store_name}}</td>
                                <td>{{$item->last_location->location->store_name}}</td>
                                <td>{{$item->it_asset_code.$item->increment_id}}</td>
                                <td>{{$item->fam_code}}</td>
                                <td>{{$item->serial_number}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

  
</div>

@endsection
