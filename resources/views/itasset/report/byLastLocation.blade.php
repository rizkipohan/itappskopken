@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Select the Last Location</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <form action="{{route('databylastlocation')}}" method="POST" autocomplete="off">
                @csrf
                <div class="form-group mb-3">
                    <select name="location" class="form-control" id="from">
                        <option value=""></option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>

</div>

<script>
    $.get("{{route('get_location')}}",
        function(data){
            $("#from").append(data);
    });
    $("#from").select2({ selectOnClose: true, theme: "bootstrap"});
</script>

@endsection