@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Transaction In</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-striped text-center" id="data-table">
                    <thead>
                        <th>#</th>
                        <th>From Location</th>
                        <th>Date</th>
                        <th>Created by</th>
                    </thead>
                    <tbody>
                        @foreach ($items->itemin as $key => $item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>
                                    @isset($item->location)
                                        {{$item->location->store_name}}
                                    @endisset
                                </td>
                                <td>{{$item->created_at->format('d/m/Y')}}</td>
                                <td>{{$item->createdby->name}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mt-3">
        <div class="col-md-4">
            <h3 class="text-center">Transaction Out</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-striped text-center" id="data-table">
                    <thead>
                        <th>#</th>
                        <th>To Location</th>
                        <th>Date</th>
                        <th>Created by</th>
                    </thead>
                    <tbody>
                        @foreach ($items->itemout as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->location->store_name}}</td>
                            <td>{{$item->created_at->format('d/m/Y')}}</td>
                            <td>{{$item->createdby->name}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
