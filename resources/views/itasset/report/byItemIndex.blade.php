@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Item List</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped text-center" id="data-table">
                    <thead>
                        <th>#</th>
                        <th>Item</th>
                        <th>Origin</th>
                        <th>IT Asset Code</th>
                        <th>FAM Code</th>
                        <th>Serial Number</th>
                        <th></th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(function() {
        var oTable = $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            dom: '<"html5buttons">Blfrtip',
            buttons : [
                {
                    extend: 'excel', 
                    title:'',
                    exportOptions: {
                        format: {
                            header: function (data,index) {
                                return data.split(' ').join('_');
                            }
                        }
                    }
                },
            ],
            ajax: {
                url: '{{route('datareportbyitem')}}'
            },
            columns: [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'itemname.name', name: 'itemname.name', render: function (data,type,row){
                        return (data == null) ? "" : row.itemtype.name+" "+row.itemname.name;
                    } 
                },
                {data: 'location.store_name', name: 'location.store_name', render: function (data,type,row){
                        return (data == null) ? "" : row.location.store_name;
                    } 
                },
                {data: 'it_asset_code', render: function (data,type,row){
                        return (data == null) ? "" : row.it_asset_code+row.increment_id;
                    } 
                },
                {data: 'fam_code', name: 'fam_code'},
                {data: 'serial_number', name: 'serial_number'},
                {data: 'btn-view-trx', name: 'btn-view-trx', orderable: false, searchable: false},
            ]
        });
    });
</script>
@endsection
