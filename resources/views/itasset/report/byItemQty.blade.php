@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center"></h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-striped text-center" id="data-table">
                    <thead>
                        <th>#</th>
                        <th>Item</th>
                        <th>Available</th>
                        <th>Deployed</th>
                        <th>Total</th>
                    </thead>
                    <tbody>
                        @foreach ($items as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->itemtype->name." ".$item->name}}</td>
                            <td>{{$item->itemdetail->where('is_stock',1)->count()}}</td>
                            <td>{{$item->itemdetail->where('is_stock',0)->count()}}</td>
                            <td>{{$item->itemdetail->count()}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection
