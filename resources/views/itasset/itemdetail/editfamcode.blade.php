@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Edit FAM Code And Serial Number</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <form action="{{url('itasset/itemdetail/update/famcode/'.$item->id)}}" method="POST">
                @csrf
                <div class="form-group mb-3">
                    <label for="">Item Name:</label>
                    <select name="" id="" class="form-select" disabled>
                        <option value="{{$item->id}}" selected>{{$item->itemtype->name." ".$item->itemname->name}}</option>
                    </select>                
                </div>

                <div class="form-group mb-3">
                    <label for="">IT Asset Code</label>
                    <input type="text" name="" class="form-control" id="" value="{{$item->it_asset_code.$item->increment_id}}" readonly disabled>
                </div>

                <div class="form-group mb-3">
                    <label for="">FAM Code</label>
                    <input type="text" name="fam_code" class="form-control" id="" value="{{$item->fam_code}}">
                </div>

                <div class="form-group mb-3">
                    <label for="">Serial Number</label>
                    <input type="text" name="serial_number" class="form-control" id="" value="{{$item->serial_number}}">
                </div>

                <div class="form-group ">
                    <button type="submit" class="btn btn-success btn-sm">Update</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection