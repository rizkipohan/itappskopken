@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Item For Store Section</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped text-center" id="data-table">
                    <thead>
                        <th>#</th>
                        <th>Item Type</th>
                        <th>Item Name</th>
                        <th>Origin</th>
                        <th>Last Location</th>
                        <th>Asset Code</th>
                        <th>FAM Code</th>
                        <th>S/N</th>
                        <th>Available</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(function() {
        var oTable = $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            dom: '<"html5buttons">Blfrtip',
            buttons : [
                {
                    extend: 'excel', 
                    title:'',
                    exportOptions: {
                        format: {
                            header: function (data,index) {
                                return data.split(' ').join('_');
                            }
                        }
                    }
                },
            ],
            ajax: {
                url: '{{ url("itasset/itemdetail/dataitemdetail") }}'
            },
            columns: [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'itemtype.name', name: 'itemtype.name'},
                {data: 'itemname.name', name: 'itemname.name'},
                {data: 'location.store_name', name: 'location.store_name', render: function (data,type,row){
                        return (data == null) ? "" : row.location.store_name;
                    } 
                },
                {data: 'last_location',  name: 'last_location', render: function (data,type,row){
                        return (data == null) ? "" : row.last_location;
                    } 
                },
                {data: 'it_asset_code', name:'it_asset_code', render: function (data,type,row){
                        return (data == null) ? "" : row.it_asset_code+row.increment_id;
                    } 
                },
                {data: 'fam_code', name: 'fam_code'},
                {data: 'serial_number', name: 'serial_number'},
                {data: 'is_stock', name: 'is_stock', render: function (data,type,row){
                        return (data == 1) ? "<button class='btn btn-success'>Yes</button>" : "<button class='btn btn-danger'>No</button>";
                    } 
                },
                {data: 'btn-fam', name: 'btn-fam', orderable: false, searchable: false},
                {data: 'btn-cancel', name: 'btn-cancel', orderable: false, searchable: false,
                    render: function (data,type,row){
                        return (row.is_checkout == 1) ? "Checked Out" : data;
                    } 
                },
                {data: 'btn-delete', name: 'btn-delete', orderable: false, searchable: false},

                // {data: 'button', name: 'button', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection
