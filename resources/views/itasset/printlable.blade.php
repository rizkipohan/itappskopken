<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body{
            margin:0mm;
        }

        .label{
            box-sizing: border-box;
            font-size: 3mm;
            width: 40mm;
            height: 20mm;
            padding-left: 1mm;
            padding-right: 1mm;

            outline: 1px dotted;
        }

        .column {
            float: left;
            width: 50%;
        }

        .row:after {
            content: "";
            display: table;
            clear: both;
        }
      </style>

</head>
<body>

    <div class="label row">
        <div class="column">
            {{$data->itemname->name}}
            <br>
            {{$data->itemtype->name}}
            <br>
            <br>
            @if ($data->fam_code != null)
                {!! DNS1D::getBarcodeHTML($data->fam_code,'C128',0.70,20) !!}
                {{$data->fam_code}}
            @endif
        </div>
        <div class="column">
            <div style="
                padding-top: 2mm;
                padding-left: 2mm;
                margin: 0 auto;
            ">
            @if ($data->it_asset_code != null)
                {!! DNS2D::getBarcodeHTML($data->it_asset_code.$data->increment_id,'QRCODE',2.50,2.50) !!}
                {{$data->it_asset_code.$data->increment_id}}
            @endif
            </div>
        </div>
    </div>
</body>
</html>
