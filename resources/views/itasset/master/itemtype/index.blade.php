@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <h3>ITEM TYPE</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalitemtype">
                ADD
            </button>
        </div>
    </div>

    <div class="modal fade" id="modalitemtype" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Item Type</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <input type="text" name="" class="form-control" id="itemtypename" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" id="codeforasset" placeholder="Code for IT Asset">
                    </div>
                    <div class="form-group d-grid mt-2">
                        <button type="button" id="btnadditemtype" class="btn btn-success">
                            Add
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table text-center">
                <thead>
                    <tr>
                        <th>no</th>
                        <th>name</th>
                        <th>code in asset</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($gets as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->code_for_asset}}</td>
                            <td>
                                <a href="{{url('itasset/master/itemtype/delete/'.$item->id)}}" class="btn btn-danger">Delete</a>
                                <a href="{{url('itasset/master/itemtype/edit/'.$item->id)}}" class="btn btn-warning">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

    var x = $('#itemtype');
    // rfshItemType();
    // function rfshItemType(){
    //     x.empty();
    //     $.get("{{url('/itasset/jq/getitemtype')}}",
    //     function(data) {
    //         x.append(data);
    //     });
    //     x.select2({ selectOnClose: true, theme: "bootstrap"});
    // }
    $('#btnadditemtype').click(function(){
        var vx = $('#itemtypename');
        var vy = $('#codeforasset');
        $.post("{{url('/itasset/jq/additemtype')}}",
        {
            name: vx.val(),
            code_for_asset: vy.val(),
        },
        function(data, status){
            // rfshItemType();
            location.reload();
            vx.empty();
            alert("Status: " + status);
        });
    });

});
</script>

@endsection
