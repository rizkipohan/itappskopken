@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <h3>ITEM TYPE</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6 text-center">
            <form action="{{url('itasset/master/itemtype/update/'.$items->id)}}" method="POST">
                @csrf
                <div class="form-group mb-3">
                    <input type="text" name="name" class="form-control" id="" placeholder="Name" value="{{$items->name}}">
                </div>
                <div class="form-group">
                    <input type="text" name="code_for_asset" class="form-control" id="" placeholder="Code for IT Asset" value="{{$items->code_for_asset}}">
                </div>
                <div class="form-group d-grid mt-2">
                    <button type="submit" id="btnadditemtype" class="btn btn-success">
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
