@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <h3>Location</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modallocation">
                ADD
            </button>
        </div>
    </div>

    <div class="modal fade" id="modallocation" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Location</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <input type="text" name="" class="form-control" id="locationname" placeholder="Name">
                    </div>
                    <div class="form-group mb-3">
                        <input type="text" name="" class="form-control" id="storecode" placeholder="Store Code">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" id="codeforasset" placeholder="Code for IT Asset">
                    </div>
                    <div class="form-group d-grid mt-2">
                        <button type="button" id="btnadd" class="btn btn-success">
                            Add
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table text-center" id="data-table">
                <thead>
                    <tr>
                        <th>no</th>
                        <th>name</th>
                        <th>store_code</th>
                        <th>code in asset</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
$(function() {
    var oTable = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ url("itasset/master/location/datalocationindex") }}'
        },
        columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'store_name', name: 'store_name'},
            {data: 'store_code', name: 'store_code'},
            {data: 'code_for_asset', name: 'code_for_asset'},
            {data: 'button', name: 'button'}
        ]
    });
});
$(document).ready(function(){

    $('#btnadd').click(function(){
        var vx = $('#locationname');
        var vy = $('#codeforasset');
        var vz = $('#storecode');
        $.post("{{url('/itasset/jq/addlocation')}}",
        {
            name: vx.val(),
            code_for_asset: vy.val(),
            store_code: vz.val(),
        },
        function(data, status){
            location.reload();
            vx.empty();
            vz.empty();
            alert("Status: " + status);
        });
    });

});
</script>

@endsection
