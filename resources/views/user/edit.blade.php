@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit credential for user: <b>{{ $user->name }}</b></div>

                <div class="card-body">
                   <form action="{{url('/superadmin/user/update/'.$user->id)}}" method="POST">
                        @csrf
                        
                            <div class="form-group">
                                @foreach ($roles as $role)
                                <div class="form-check form-check-inline">
                                    @if ($user->role_id == $role->id)
                                        <input type="radio" class="form-check-input" name="role" id="" value="{{$role->id}}" checked>
                                    @else
                                        <input type="radio" class="form-check-input" name="role" id="" value="{{$role->id}}">
                                    @endif
                                    <label class="form-check-label">
                                        {{$role->name}}
                                    </label>
                                </div>
                                @endforeach
                            </div>
                        
                        <button type="submit" class="btn btn-primary">Update</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection