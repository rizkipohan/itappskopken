@extends('requestitem.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            <br>

            <h1 class="text-center">Create Request Item Out</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-10">
            <form action="{{url('/requestitem/store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-9">
                        <label for="">Select Store</label>
                        <select name="store_id" class="custom-select" id="store_id" required>
                            <option value="" selected></option>
                            @foreach ($stores as $store)
                                <option value="{{$store->id}}">{{$store->store_name}}</option>
                            @endforeach
                            <script>
                                $("#store_id").select2({
                                    selectOnClose: true,
                                    theme: "bootstrap"
                                });
                            </script>
                        </select>
                        <input type="hidden" name="store_name" id="store_name">
                        <script type="text/javascript">
                            $("#store_id").change(function(){
                                $("#store_name").val($("#store_id option:selected").text());
                            });
                        </script>
                    </div>
                    <div class="form-group col">
                        <br>
                        <a class="btn btn-info btn-block" href="{{url('master/store/create')}}">Add New Store</a>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col">
                        <label for="">Date</label>
                        <input type="text" class="form-control" id="date" name="date" value="{{date("Y-m-d")}}">
                        <script type="text/javascript">
                            $("#date").datepicker({
                                format: 'yyyy-mm-dd',
                                orientation: 'bottom',
                                autoclose: true
                            });
                        </script>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Serial Number</th>
                                <th>Qty</th>
                                <th>Status</th>
                                {{-- <th>Date</th> --}}
                            </tr>
                        </thead>
                        <tbody id="table">
                            <tr>
                                <td>1.</td>
                                <td>
                                    <select name="item_id[0]" id="item_id[0]" class="custom-select" required>
                                        <option value="" selected></option>
                                        @foreach ($items as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    {{-- <input type="hidden" name="item_name[0]" id="item_name">
                                    <script type="text/javascript">
                                        $("#item_id\\[0\\]").change(function(){
                                            $("#item_name\\[0\\]").val($("#item_id\\[0\\] option:selected").text());
                                        });
                                    </script> --}}
                                </td>
                                <td><input type="text" class="form-control" name="serial_number[0]"></td>
                                <td><input type="number" class="form-control" name="qty[0]" min="1" value="1" required></td>
                                <td>
                                    <select name="status[0]" class="custom-select" id="from" required>
                                        {{-- <option value="">-</option> --}}
                                        <option value="nso" selected>N.S.O</option>
                                        {{-- <option value="replace">replace</option>
                                        <option value="service">service</option>
                                        <option value="employee">employee</option>
                                        <option value="others">others</option> --}}
                                    </select>
                                </td>
                                {{-- <td>
                                    <input type="text" class="form-control" id="date[0]" name="date[0]">
                                    <script type="text/javascript">
                                        $("#date[0]").datepicker({
                                            format: 'yyyy-mm-dd',
                                            orientation: 'bottom',
                                            autoclose: true
                                        });
                                    </script>
                                </td> --}}
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="form-group text-center">
                    <button type="button" class="btn btn-success mb-2" id="new">Add New Item</button><br>
                    <button type="button" class="btn btn-danger" id="delete">Remove Last Item</button>
                </div>

                <div class="form-group row justify-content-center">
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var angka = 2;

    $("#new").click(function(){
        $('#table').append(`
            <tr>
                <td>`+angka+`</td>
                <td>
                    <select name="item_id[`+(angka-1)+`]" class="custom-select" required>
                        <option value=""></option>
                       `+"{!!$opt!!}"+`
                    </select>
                </td>
                <td><input type="text" class="form-control" name="serial_number[`+(angka-1)+`]"></td>
                <td><input type="number" class="form-control" name="qty[`+(angka-1)+`] "min="1" value="1" required></td>
                <td>
                    <select name="status[`+(angka-1)+`]" class="custom-select" required>
                        <option value="nso" selected>N.S.O</option>
                    </select>
                </td>
            </tr>
        `);
        // $("#date"+(angka-1)).datepicker({
        //     format: 'yyyy-mm-dd',
        //     orientation: 'bottom',
        //     autoclose: true
        // });
        angka++;
    });

    $("#delete").click(function(){
        $('#table tr:last').remove();
        if(angka>1){
            angka--;
        }
    });
</script>

@endsection
