@extends('requestitem.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-9">
            <form action="{{url('/requestitem/update/'.$id)}}">
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Requested By :</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="{{$reqs->user->name}}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">For Store :</label>
                    <div class="col-sm-10">
                        <input type="hidden" name="store_id" value="{{$reqs->store_id}}">
                        <input type="hidden" name="date" value="{{$reqs->date}}">
                        <input type="text" name="" class="form-control" value="{{$reqs->store->store_name}}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Item :</label>
                </div>
                <table class="table table-bordered text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>name</th>
                            <th>qty</th>
                            <th>serial number</th>
                            <th>status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $key => $item)
                        <input type="hidden" name="item_id[{{$key}}]" value="{{$item['item_id']}}">
                        <input type="hidden" name="item_name[{{$key}}]" value="{{$item['item_name']}}">
                        <input type="hidden" name="qty[{{$key}}]" value="{{$item['qty']}}">
                        <input type="hidden" name="serial_number[{{$key}}]" value="{{$item['serial_number']}}">
                        <input type="hidden" name="status[{{$key}}]" value="{{$item['status']}}">
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item['item_name']}}</td>
                            <td>{{$item['qty']}}</td>
                            <td>{{$item['serial_number']}}</td>
                            <td>{{$item['status']}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="form-group row justify-content-center">
                    <h3>Approve?</h3>
                </div>

                <div class="form-group row justify-content-center">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="approval" id="inlineRadio1" value="1">
                        <label class="form-check-label" for="inlineRadio1">Approve</label>
                    </div>
                    {{-- <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="approval" id="inlineRadio2" value="2">
                        <label class="form-check-label" for="inlineRadio2">Reject</label>
                    </div> --}}
                </div>

                <div class="form-group row justify-content-center">
                    <button type="submit" class="col-lg-6 btn btn-primary btn-block">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection