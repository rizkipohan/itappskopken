@extends('master.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="">
                <div class=""></div>

                <div class="">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                   <form action="{{ url('master/store/storing') }}" method="POST" class="" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="">Store Name</label>
                            <input type="text" class="form-control" name="store_name" id="" required>
                        </div>

                        <div class="form-group">
                            <label for="">Store Code</label>
                            <input type="text" class="form-control" name="store_code" id="" required>
                        </div>

                        <div class="form-group">
                            <label for="">Store Status</label>
                            <select class="custom-select" name="store_status" id="">
                                <option value="Close">Close</option>
                                <option value="Open" selected>Open</option>
                                {{-- <option value="2">Disabled</option> --}}
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="" class="">Open Date</label>
                            <div class="">
                                <input type="text" class="form-control" id="open_date" name="open_date">
                                <script type="text/javascript">
                                    $("#open_date").datepicker({
                                        format: 'yyyy-mm-dd',
                                        orientation: 'bottom',
                                        autoclose: true
                                    });
                                </script>
                            </div>
                        </div>

                        <div class="form-group">
                          <label for="">Store Address</label>
                          <textarea class="form-control" name="store_address" id="" rows="3"></textarea>
                        </div>

                        <div class="form-group">
                          <label for="">Store Email</label>
                          <input type="email" class="form-control" name="store_email" id="">
                        </div>

                        <div class="form-group">
                          <label for="">Store Phone</label>
                          <input type="text" name="store_phone" id="" class="form-control">
                        </div>

                        {{-- <div class="form-group">
                            <label for="">Tax Number (NOPD)</label>
                            <input type="text" name="tax_number" id="" class="form-control">
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="">Connection Status</label>
                            <select name="connection_status" class="custom-select" id="">
                                <option value=""></option>
                                <option value="On Provisioning">On Provisioning</option>
                                <option value="On Activation">On Activation</option>
                                <option value="Online">Online</option>
                            </select>
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="">Internet Provider</label>
                            <input type="text" name="provider" class="form-control" id="">
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="">SID</label>
                            <input type="text" name="sid" id="" class="form-control">
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="">Mikrotik Status</label>
                            <select name="mikrotik_status" class="custom-select" id="">
                                <option value=""></option>
                                <option value="Sudah Dikirim">Sudah Dikirim</option>
                            </select>
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="">IP Local</label>
                            <input type="text" name="ip_local" id="" class="form-control">
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="">IP WAN</label>
                            <input type="text" name="ip_wan" id="" class="form-control" >
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="">IP Gateway WAN</label>
                            <input type="text" name="ip_gateway_wan" id="" class="form-control" >
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="">IP DNS</label>
                            <input type="text" name="ip_dns" id="" class="form-control" >
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="">IP VPN</label>
                            <input type="text" name="ip_vpn" id="" class="form-control">
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="" class="">Online Date</label>
                            <div class="">
                                <input type="text" class="form-control" id="online_date" name="online_date">
                                <script type="text/javascript">
                                    $("#online_date").datepicker({
                                        format: 'yyyy-mm-dd',
                                        orientation: 'bottom',
                                        autoclose: true
                                    });
                                </script>
                            </div>
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="" class="">Active Date</label>
                            <div class="">
                                <input type="text" class="form-control" id="active_date" name="active_date">
                                <script type="text/javascript">
                                    $("#active_date").datepicker({
                                        format: 'yyyy-mm-dd',
                                        orientation: 'bottom',
                                        autoclose: true
                                    });
                                </script>
                            </div>
                        </div> --}}

                        <button type="submit" class="btn btn-primary btn-block">Create</button>

                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
