@extends('master.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">Store List</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="">
               
                <div class="form-inline my-3 justify-content-end">
                    <a name="" id="" class="btn btn-success mx-2" href="{{url('master/store/create')}}" role="button">Add</a>
                
                    {{-- <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal">
                        <i class="fa fa-search" aria-hidden="true"></i> Search 
                    </button> --}}

                    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Search</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{url('master/store/search')}}" method="get" autocomplete="off">
                                        
                                        <div class="form-group row mb-3">
                                            <label for="startDate" class="col-md-4 col-form-label text-md-right">
                                                Store Name or Stor Code
                                            </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="search" id="" aria-describedby="helpId" placeholder="">
                                            </div>
                                        </div>

                                        <div class="form-group row justify-content-center">
                                            <button type="submit" class="col-md-6 btn btn-success btn-block">Search</button>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                {{-- <div class="form-inline my-3 justify-content-center">
                    {{$stores->links()}}
                </div> --}}
                
                {{-- <div class="table-responsive"> --}}
                    <table class="table table-bordered table-hover table-striped" id="table">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Code</th>
                                    {{-- <th>N.O.P.D</th> --}}
                                    <th>Connection Status</th>
                                    <th>Provider</th>
                                    <th>SID</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($stores)==0)
                                   <tr class="text-center"> <td colspan="100">No Data</td> </tr>
                                @else
                                    @foreach ($stores as $store)
                                    <tr class="text-center">
                                        <td>{{$store->id}}</td>
                                        <td>{{$store->store_name}}</td>
                                        <td>{{$store->store_code}}</td>
                                        {{-- <td>{{$store->tax_number}}</td> --}}
                                        <td>{{$store->connection_status}}</td>
                                        <td>{{$store->provider}}</td>
                                        <td>{{$store->sid}}</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ url('master/store/edit/'.$store->id) }}">Edit Details</a> 
                                            <a class="btn btn-danger" href="{{url('master/store/delete/'.$store->id)}}">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                    </table>
                {{-- </div> --}}

            </div>
        </div>
    </div>
</div>

<script>
    // const queryString = window.location.search;
    // const urlParams = new URLSearchParams(queryString);
    // const page = urlParams.get('page');
    // document.cookie = "pageStore=" + page;
    $(document).ready(function(){
        $('#table').DataTable();
    });
</script>

@endsection
