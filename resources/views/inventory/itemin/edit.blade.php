@extends('inventory.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">Item In Transaction</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                   <form action="{{url('/inventory/itemin/update/'.$itemin->id)}}" method="POST" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="">Item Name</label>
                            <select name="item" class="custom-select" id="" required>
                            @foreach ($items as $item)
                                <option value="{{$item->id}}"
                                @if($item->id == $itemin->item_id)
                                selected
                                @endif>{{$item->name}}</option>
                            @endforeach
                            </select> 
                        </div>
                        <div class="form-group">
                            <label for="">Qty</label>
                            <input type="number" class="form-control" name="qty" id="" min="0" required value="{{$itemin->qty}}">
                        </div>
                        <div class="form-group">
                            <label for="">From</label>
                            <select name="from" class="custom-select" id="from" required>
                                <option value=""></option>
                                
                                <option value="store"
                                @if($itemin->from == 'store')
                                selected
                                @endif>store</option>

                                <option value="vendor"
                                @if($itemin->from == 'vendor')
                                selected
                                @endif>vendor</option>

                                <option value="service"
                                @if($itemin->from == 'service')
                                selected
                                @endif>service</option>

                                <option value="warehouse"
                                @if($itemin->from == 'warehouse')
                                selected
                                @endif>warehouse</option>

                                <option value="others"
                                @if($itemin->from == 'others')
                                selected
                                @endif>others</option>
                            </select>
                        </div>
                      
                        <div class="form-group" id="store">
                            <label for="">Store</label>
                            <select name="store" id="store" class="custom-select">
                                <option value=""></option>
                            @foreach ($stores as $store)
                                <option value="{{$store->id}}"
                                    @if($store->id == $itemin->store_id) 
                                    selected   
                                    @endif
                                >{{$store->store_name}}</option>
                            @endforeach
                            </select>
                            <script>
                                $("*[name='store']").select2({
                                    selectOnClose: true,
                                    theme: "bootstrap"
                                });

                                $("#from").change(function(){
                                    var x = $(this).children("option:selected").val();
                                    if(x != "store"){
                                        $("#store").val("");
                                    }
                                    // else{
                                    //     $("#store").show('');
                                    // }
                                });
                            </script>
                        </div>
                        <div class="form-group" id="vendor">
                            <label for="">Vendor</label>
                            <input type="text" name="vendor" class="form-control" id="" value="{{$itemin->vendor}}">
                        </div>
                        <div class="form-group">
                            <label for="">PO Number</label>
                            <input type="text" name="po_number" class="form-control" id="" value="{{$itemin->po_number}}">
                        </div>
                        <div class="form-group">
                            <label for="">PR Number</label>
                            <input type="text" name="pr_number" class="form-control" id="" value="{{$itemin->pr_number}}">
                        </div>
                        <div class="form-group">
                            <label for="">Serial Number</label>
                            <input type="text" name="serial_number" class="form-control" id="" value="{{$itemin->serial_number}}">
                        </div>
                        <div class="form-group">
                            <label for="">Notes</label>
                            <input type="text" name="notes" class="form-control" id="" value="{{$itemin->notes}}">
                        </div>
                        <div class="form-group">
                            <label for="">Date</label>
                            <input type="text" class="form-control" id="date" name="date" value="{{$itemin->date}}">
                            <script type="text/javascript">
                                $("#date").datepicker({
                                    format: 'yyyy-mm-dd',
                                    orientation: 'bottom',
                                    autoclose: true
                                });
                            </script>
                        </div>
                        <div class="row justify-content-center">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection