@extends('inventory.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">Item In Transaction</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                   <form action="{{url('/inventory/itemin/store')}}" method="POST" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="">Item Name</label>
                            <select name="item" class="custom-select" id="" required>
                                <option value="" selected></option>
                            @foreach ($items as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Qty</label>
                            <input type="number" class="form-control" name="qty" id="" min="0" required>
                        </div>
                        <div class="form-group">
                            <label for="">From</label>
                            <select name="from" class="custom-select" id="from" required>
                                <option value="" selected></option>
                                <option value="store">store</option>
                                <option value="vendor">vendor</option>
                                <option value="service">service</option>
                                <option value="warehouse">warehouse</option>
                                <option value="others">others</option>
                            </select>
                        </div>
                        <script>
                            $(document).ready(function(){
                                $("#from").change(function(){
                                    var x = $(this).children("option:selected").val();
                                    if(x == "store"){
                                        $("#store").show();
                                        $("#vendor").hide();
                                        // $("#warehouse").hide();
                                    }else if(x == "vendor"){
                                        $("#store").hide();
                                        $("#vendor").show();
                                        // $("#warehouse").hide();
                                    }else if(x == "service"){
                                        $("#store").hide();
                                        $("#vendor").show();
                                        // $("#warehouse").hide();
                                    }else if(x == "warehouse"){
                                        $("#store").hide();
                                        $("#vendor").hide();
                                        // $("#warehouse").show();
                                    }else{
                                        $("#store").hide();
                                        $("#vendor").hide();
                                        // $("#warehouse").hide();
                                    }
                                });
                            });
                        </script>
                        <div class="form-group" id="store" style="display:none;">
                            <label for="">Store</label>
                            <select name="store" class="custom-select">
                                <option value="" selected></option>
                            @foreach ($stores as $store)
                                <option value="{{$store->id}}">{{$store->store_name}}</option>
                            @endforeach
                            </select>
                            <script>
                                $("*[name='store']").select2({
                                    selectOnClose: true,
                                    theme: "bootstrap"
                                });
                            </script>
                        </div>
                        <div class="form-group" id="vendor" style="display:none;">
                            <label for="">Vendor</label>
                            <input type="text" name="vendor" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">PO Number</label>
                            <input type="text" name="po_number" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">PR Number</label>
                            <input type="text" name="pr_number" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Serial Number</label>
                            <input type="text" name="serial_number" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Notes</label>
                            <input type="text" name="notes" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Date</label>
                            <input type="text" class="form-control" id="date" name="date">
                            <script type="text/javascript">
                                $("#date").datepicker({
                                    format: 'yyyy-mm-dd',
                                    orientation: 'bottom',
                                    autoclose: true
                                });
                            </script>
                        </div>
                        <button type="submit" class="btn btn-success">Create</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection