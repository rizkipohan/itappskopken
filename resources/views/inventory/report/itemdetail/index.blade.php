@extends('inventory.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">Select Item to Export the Detail Transaction</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4 mb-2">

         {{-- <form action="{{url('/inventory/report/itemdetail/export')}}" method="POST">
             @csrf
             <div class="form-group">
                 <label for="">Select Item</label>
                 <select name="item" id="" class="custom-select">
                     <option value="" selected></option>
                     @foreach ($items as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                     @endforeach
                 </select>
             </div>
             <button class="btn btn-block btn-primary" type="submit">submit</button>
         </form> --}}

         <table class="table table-hover text-center">
            <thead>
                <tr>
                    <td>#</td>
                    <td>Item Name</td>
                    <td>Export</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($items as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>
                            <a href="{{url('/inventory/report/itemdetail/export/'.$item->id)}}" class="btn btn-success">Export</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>

         </table>

        </div>
    </div>
</div>
@endsection