@extends('inventory.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
    
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4 mb-2">
            <form action="{{url('inventory/report/itembalance/index')}}" method="GET">
                <div class="form-group">
                    <label for="">Select Department</label>
                    <select name="dept" class="custom-select" id="" required>
                        <option value=""></option>
                        @foreach ($dept as $dept)
                        <option value="{{$dept->id}}">{{$dept->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-primary">Show Report</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection