
<table>
    <thead>
        <tr >
            <th>#</th>
            <th>Item Name</th>
            <th>In</th>
            <th>Out</th>
            <th>Balance</th>
        </tr>
    </thead>
    <tbody>
        @if(count($items)==0)
        <tr><td colspan="100">No Data</td></tr>
        @else
            @foreach ($items as $key => $item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->itemin->sum('qty')}}</td>
                <td>{{$item->itemout->sum('qty')}}</td>
                <td>{{$item->itemin->sum('qty') - $item->itemout->sum('qty')}}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>