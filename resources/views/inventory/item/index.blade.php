@extends('inventory.app')

@section('content')
<div class="container">
    
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">Item List</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="">
                {{-- <div class="">Store List</div> --}}

                <div class="">
                    <div class="d-flex justify-content-center">
                        <a name="" id="" class="btn btn-success my-2" href="{{url('inventory/item/create')}}" role="button">Add New Item</a>
                    </div>
                    <table class="table table-hover" class="table-striped">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Item Name</th>
                                    {{-- <th>Price</th> --}}
                                    {{-- <th>Status</th> --}}
                                    <th>Qty</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($items)==0)
                                <tr class="text-center"><td colspan="100">No Data</td></tr>
                                @else
                                    @foreach ($items as $key => $item)
                                    <tr class="text-center">
                                        <td>{{$key+1}}</td>
                                        <td>{{$item->name}}</td>
                                        {{-- <td>{{$item->price}}</td> --}}
                                        {{-- <td>{{$item->show}}</td> --}}
                                        <td>{{$item->itemin->sum('qty') - $item->itemout->sum('qty')}}</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{url('inventory/item/edit/'.$item->id)}}">Edit</a> | 
                                            <a class="btn btn-danger" href="{{url('inventory/item/delete/'.$item->id)}}">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
