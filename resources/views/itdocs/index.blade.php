@extends('itdocs.app')

@section('content')
<div class="container">
    
    <div class="row">
        <div class="col-4">
            <div class="list-group" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" 
                href="#list-docs" role="tab" aria-controls="home">Documents</a>
                <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" 
                href="#list-tutorial" role="tab" aria-controls="profile">Tutorials</a>
            </div>
        </div>
        <div class="col-6">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-docs" role="tabpanel" aria-labelledby="list-home-list">
                    <h5>Choose the document</h5>
                    <ul>
                        <li><a href="{{url('/public/itdocs/docs/Formulir Permintaan Hak Akses Personil.docx')}}"  target="_blank">
                            Formulir Permintaan Hak Akses Personil</a>
                        </li>
                        <li><a href="{{url('/public/itdocs/docs/Berita Acara Pengembalian Asset.docx')}}"  target="_blank">
                            Berita Acara Pengembalian Asset</a>
                        </li>
                        <li><a href="{{url('/public/itdocs/docs/Berita Acara Serah Terima Barang.docx')}}"  target="_blank">
                            Berita Acara Serah Terima Barang</a>
                        </li>
                        <li><a href="{{url('/public/itdocs/docs/Formulir Laporan Kerusakan Aset 2.03.docx')}}"  target="_blank">
                            Formulir Laporan Kerusakan Aset</a>
                        </li>
                        <li><a href="{{url('/public/itdocs/docs/Formulir Pemeliharaan Perangkat.docx')}}"  target="_blank">
                            Formulir Pemeliharaan Perangkat</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="list-tutorial" role="tabpanel" aria-labelledby="list-profile-list">
                    <h5>Choose the tutorial</h5>
                    <ul>
                        <li><a href="{{url('/public/itdocs/tutorial/Anydesk.pdf')}}" target="_blank">
                            Anydesk</a>
                        </li>
                        <li><a href="{{url('/public/itdocs/tutorial/LDAP.pdf')}}"  target="_blank">
                            LDAP</a>
                        </li>
                        <li><a href="{{url('/public/itdocs/tutorial/Google Customization Bahasa.pdf')}}"  target="_blank">
                            Google Customization Bahasa</a>
                        </li>
                        <li><a href="{{url('/public/itdocs/tutorial/Google Customization English.pdf')}}"  target="_blank">
                            Google Customization English</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
    </div>


</div>
@endsection
