@extends('lnd.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            
               
                @if ($files->format == "mp4" || $files->format == "mpeg4" || $files->format == "mov" || $files->format == "flv" || 
                $files->format == "avi" || $files->format == "flv" || $files->format == "wmv")
               
                    <video controls controlsList="nodownload" class="embed-responsive embed-responsive-16by9">
                        <source src="{{url('public/document/'.$files->name)}}" class="embed-responsive-item" type="{{$files->mime_type}}">
                    </video>
               
                @elseif($files->format == "pdf"  || $files->format == "txt")

                    <div class="embed-responsive embed-responsive-1by1">
                        <iframe class="embed-responsive-item" src="{{url('public/document/'.$files->name)}}#toolbar=0"></iframe>
                    </div>

                {{-- @elseif($files->format == "doc" || $files->format == "docx" || $files->format == "xls" || $files->format == "xlsx" || 
                $files->format == "csv"|| $files->format == "ppt" || $files->format == "pptx") --}}

                @else
               
                    <img src="{{url('public/document/'.$files->name)}}" class="mx-auto d-block" alt="">
               
                @endif
               
            
        </div>
    </div>
</div>
@endsection
