@extends('lnd.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">File list in {{$folder->name}} folder</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="">
    
                <div class="form-inline my-3 justify-content-center">
                    {{-- @if (Auth::user()->department_id == 2 || Auth::user()->department_id == 7 )
                        <a name="" id="" class="btn btn-success mx-2" href="{{url('/file/create')}}" role="button">Upload a New File</a>
                    @endif --}}

                    {{$files->links()}}
                </div>

                <div class="row mx-2">
                    <small class="col">*click the file name if you want to see the file</small>
                </div>
                
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped ">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>File Name</th>
                                    <th>Format</th>
                                    <th>Uploaded At</th>
                                    {{-- <th>Action</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($files)==0)
                                   <tr class="text-center"> <td colspan="100">No Data</td> </tr>
                                @else
                                    @foreach ($files as $file)
                                    <tr class="text-center">
                                        <td>{{$file->id}}</td>
                                        <td><a href="{{url('file/show/'.$file->id)}}">{{$file->name}}</a></td>
                                        <td>{{$file->format}}</td>
                                        <td>{{$file->created_at}}</td>
                                        {{-- <td>
                                            <a class="btn btn-danger" href="{{url('/#')}}">delete</a>
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
