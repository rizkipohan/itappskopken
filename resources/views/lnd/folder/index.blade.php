@extends('lnd.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">Uploaded File List</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="">
    
                <div class="form-inline my-3 justify-content-center">
                    @if (Auth::user()->department_id == 2 || Auth::user()->department_id == 7 )
                        <a name="" id="" class="btn btn-success mx-2" href="{{url('/folder/create')}}" role="button">Create Folder</a>
                    @endif

                    {{$folders->links()}}
                </div>
                
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped ">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Folder Name</th>
                                    {{-- <th>Action</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($folders)==0)
                                   <tr class="text-center"> <td colspan="100">No Data</td> </tr>
                                @else
                                    @foreach ($folders as $folders)
                                    <tr class="text-center">
                                        <td>{{$folders->id}}</td>
                                        <td><a href="{{url('/folder/file/index/'.$folders->id)}}">{{$folders->name}}</a></td>
                                        {{-- <td>
                                            <a class="btn btn-danger" href="{{url('/#')}}">delete</a>
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
