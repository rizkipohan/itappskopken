
Dear NSO Team,

Berikut ini adalah report NSO selama satu bulan yang lalu:

<table>
    <tr>
        <td>#</td>
        <td>Store Name</td>
        <td>Opening Date</td>
        <td>Store Code</td>
        <td>Created By</td>
    </tr>
    @foreach ($data as $key => $item)
    <tr>
        <td>{{$key+1}}</td>
        <td>{{$item->store['store_name']}}</td>
        <td>{{date('d-M-Y', strtotime($item->open_date))}}</td>
        <td>{{$item->apps_id}}</td>
        <td>{{$item->created_by_user['name']}}</td>
    </tr>
    @endforeach
</table>
