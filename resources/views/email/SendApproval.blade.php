@component('mail::message')

Dear {{$user->name}},

Permintaan item anda untuk:

@component('mail::table')

<table>
   <tr>
       <td>Toko</td>
       <td>:</td>
       <td>{{$reqs->store->store_name}}</td>
   </tr>
   <tr>
       <td>Item</td>
       <td colspan="2">:</td>
   </tr>
</table>
<table style="text-align: center;">
    <tr>
        <th>no</th>
        <th>nama</th>
        <th>qty</th>
        <th>serial number</th>
    </tr>
    @foreach($items as $key => $item)
     <tr>
         <td>{{$key+1}}</td>
         <td>{{$item['item_name']}}</td>
         <td>{{$item['qty']}}</td>
         <td>{{$item['serial_number']}}</td>
     </tr>
    @endforeach
</table>
@endcomponent

<h1 style="text-align:center;">Telah diterima oleh admin</h1>

@endcomponent