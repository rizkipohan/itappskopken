@extends('nso.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="">
                <div class=""></div>

                <div class="">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    
                    <h4 class="text-center">Edit NSO PDF</h4>

                    <form action="{{ url('/nso/update/'.$nso->id) }}" method="POST" onsubmit="onFormSubmit()" class="" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-9">
                                <label for="">Select Store</label>
                                <select name="store_id" class="custom-select" id="store_id" required>
                                    @foreach ($stores as $store)
                                        <option value="{{$store->id}}"
                                        @if ($store->id == $nso->store_id)
                                            selected
                                        @endif >
                                        {{$store->store_name}} ( {{$store->store_code}} )</option>
                                    @endforeach
                                    <script>
                                        $("#store_id").select2({
                                            selectOnClose: true,
                                            theme: "bootstrap"
                                        });
                                    </script>
                                </select>
                            </div>
                            <div class="form-group col">
                                <br>
                                <a name="" id="" class="btn btn-success btn-block" href="{{url('master/store/create')}}">Add New Store</a>  
                            </div>          
                        </div>

                        <div class="form-group">
                            @isset($item->img)
                            <label for="">Existing file:</label> 
                                <br>
                                <img src="{{$item->img}}" class="img-fluid" alt="">
                                @isset($item->img)
                                    <input type="hidden" name="img" value="{{$item->img}}">
                                @endisset
                                @isset($item->img_name)
                                    <input type="hidden" name="img_name" value="{{$item->img_name}}">
                                @endisset
                                <br>
                            <label>Or upload new file</label>
                            @endisset
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="layout" name="layout_edit" >
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                </div>
                                <script>
                                    $('#layout').on('change',function(){
                                        var fileName = $(this).val();
                                        $(this).next('.custom-file-label').html(fileName);
                                    })
                                </script>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="">IT Equipment</label>
                        </div>

                        <table class="table table-hover table-bordered text-center">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Name</td>
                                    <td>Quantity</td>
                                    <td>Serial Number</td>
                                    <td>Available</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>POS</td>
                                    <td><input type="number" name="pos_quantity" class="form-control" id="" min="0" value="{{$item->pos_quantity}}"></td>
                                    <td><input type="text" name="pos_sn" class="form-control" id="" value="{{$item->pos_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="pos_check" id="" value="1" @isset($item->pos_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Drawer</td>
                                    <td><input type="number" name="drawer_quantity" class="form-control" id="" min="0" value="{{$item->drawer_quantity}}"></td>
                                    <td><input type="text" name="drawer_sn" class="form-control" id="" value="{{$item->drawer_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="drawer_check" id="" value="1" @isset($item->drawer_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Laptop</td>
                                    <td><input type="number" name="laptop_quantity" class="form-control" id="" min="0" value="{{$item->laptop_quantity}}"></td>
                                    <td><input type="text" name="laptop_sn" class="form-control" id="" value="{{$item->laptop_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="laptop_check" id="" value="1"  @isset($item->laptop_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Scanner</td>
                                    <td><input type="number" name="scanner_quantity" class="form-control" id="" min="0" value="{{$item->scanner_quantity}}"></td>
                                    <td><input type="text" name="scanner_sn" class="form-control" id="" value="{{$item->scanner_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="scanner_check" id="" value="1"  @isset($item->scanner_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Tablet</td>
                                    <td><input type="number" name="tablet_quantity" class="form-control" id="" min="0" value="{{$item->tablet_quantity}}"></td>
                                    <td><input type="text" name="tablet_sn" class="form-control" id="" value="{{$item->tablet_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="tablet_check" id="" value="1"  @isset($item->tablet_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Handphone</td>
                                    <td><input type="number" name="hp_quantity" class="form-control" id="" min="0" value="{{$item->hp_quantity}}"></td>
                                    <td><input type="text" name="hp_sn" class="form-control" id="" value="{{$item->hp_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="hp_check" id="" value="1" @isset($item->hp_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Printer Thermal</td>
                                    <td><input type="number" name="thermal_quantity" class="form-control" id="" min="0" value="{{$item->thermal_quantity}}"></td>
                                    <td><input type="text" name="thermal_sn" class="form-control" id="" value="{{$item->thermal_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="thermal_check" id="" value="1" @isset($item->thermal_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Mikrotik</td>
                                    <td><input type="number" name="mikrotik_quantity" class="form-control" id="" min="0" value="{{$item->mikrotik_quantity}}"></td>
                                    <td><input type="text" name="mikrotik_sn" class="form-control" id="" value="{{$item->mikrotik_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="mikrotik_check" id="" value="1" @isset($item->mikrotik_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Camera</td>
                                    <td><input type="number" name="camera_quantity" class="form-control" id="" min="0" value="{{$item->camera_quantity}}"></td>
                                    <td><input type="text" name="camera_sn" class="form-control" id="" value="{{$item->camera_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="camera_check" id="" value="1" @isset($item->camera_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>DVR</td>
                                    <td><input type="number" name="dvr_quantity" class="form-control" id="" min="0" value="{{$item->dvr_quantity}}"></td>
                                    <td><input type="text" name="dvr_sn" class="form-control" id="" value="{{$item->dvr_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="dvr_check" id="" value="1" @isset($item->dvr_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td>Printer Label (Bixolon)</td>
                                    <td><input type="number" name="label_quantity" class="form-control" id="" min="0" value="{{$item->label_quantity}}"></td>
                                    <td><input type="text" name="label_sn" class="form-control" id="" value="{{$item->label_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="label_check" id="" value="1" @isset($item->label_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>12</td>
                                    <td>T-Hub</td>
                                    <td><input type="number" name="thub_quantity" class="form-control" id="" min="0" value="{{$item->thub_quantity}}"></td>
                                    <td><input type="text" name="thub_sn" class="form-control" id="" value="{{$item->thub_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="thub_check" id="" value="1" @isset($item->thub_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>13</td>
                                    <td>USB Extension</td>
                                    <td><input type="number" name="ext_quantity" class="form-control" id="" min="0" value="{{$item->ext_quantity}}"></td>
                                    <td><input type="text" name="ext_sn" class="form-control" id="" value="{{$item->ext_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="ext_check" id="" value="1" @isset($item->ext_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>14</td>
                                    <td>Flashdisk</td>
                                    <td><input type="number" name="fd_quantity" class="form-control" id="" min="0" value="{{$item->fd_quantity}}"></td>
                                    <td><input type="text" name="fd_sn" class="form-control" id="" value="{{$item->fd_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="fd_check" id="" value="1" @isset($item->fd_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>15</td>
                                    <td>Stand Tablet</td>
                                    <td><input type="number" name="stand_quantity" class="form-control" id="" min="0" value="{{$item->stand_quantity}}"></td>
                                    <td><input type="text" name="stand_sn" class="form-control" id="" value="{{$item->stand_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="stand_check" id="" value="1" @isset($item->stand_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>16</td>
                                    <td>UPS</td>
                                    <td><input type="number" name="ups_quantity" class="form-control" id="" min="0" value="{{$item->ups_quantity}}"></td>
                                    <td><input type="text" name="ups_sn" class="form-control" id="" value="{{$item->ups_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="ups_check" id="" value="1" @isset($item->ups_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>17</td>
                                    <td>HUB</td>
                                    <td><input type="number" name="hub_quantity" class="form-control" id="" min="0" value="{{$item->hub_quantity}}"></td>
                                    <td><input type="text" name="hub_sn" class="form-control" id="" value="{{$item->hub_sn}}"></td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="hub_check" id="" value="1" @isset($item->hub_check) checked @endisset>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <br>
                        Approved by: <br> <br>

                        <div class="row justify-content-center">
                            <div class="col-md-12">
                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <td>Store Manager</td>
                                            <td>IT Project SPV</td>
                                            {{-- <td>IT Support SPV</td> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="5">
                                                {{-- <canvas id="signature"></canvas>
                                                <input type="hidden" name="sig" id="sig">
                                                <br>
                                                <button id="clear">Clear</button>
                                                <br>
                                                <br> --}}
                                                <input type="text" name="sm" class="form-control mx-auto" id="" placeholder="Store Manager Name" value="{{$item->sm}}" required>
                                            </td>
                                            <td rowspan="5">
                                                {{-- <canvas id="signature1"></canvas>
                                                <input type="hidden" name="sig1" id="sig1">
                                                <br>
                                                <button id="clear1">Clear</button>
                                                <br>
                                                <br> --}}
                                                <input type="text" name="spv" class="form-control mx-auto" id="" placeholder="IT Project SPV Name" value="{{$item->spv}}" required>
                                            </td>
                                            {{-- <td rowspan="5">
                                                <canvas id="signature2"></canvas>
                                                <input type="hidden" name="sig2" id="sig2">
                                                <button id="clear2">Clear</button>
                                                <br>
                                                <br>
                                                <input type="text" name="support" class="form-control mx-auto" id="" placeholder="IT Support SPV Name" required>
                                            </td> --}}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $("*[type='number']").addClass('text-center');

                            var canvas = document.getElementById('signature');
                            var signaturePad = new SignaturePad(canvas,{
                                backgroundColor: 'rgb(200, 200, 200)'
                            });
                            var cancelButton = document.getElementById('clear');
                            cancelButton.addEventListener('click', function (event) {
                                signaturePad.clear();
                            });
                            
                            var canvas1 = document.getElementById('signature1');
                            var signaturePad1 = new SignaturePad(canvas1,{
                                backgroundColor: 'rgb(200, 200, 200)'
                            });
                            var cancelButton1 = document.getElementById('clear1');
                            cancelButton1.addEventListener('click', function (event) {
                                signaturePad1.clear();
                            });
                            
                            var canvas2 = document.getElementById('signature2');
                            var signaturePad2 = new SignaturePad(canvas2,{
                                backgroundColor: 'rgb(200, 200, 200)'
                            });
                            var cancelButton2 = document.getElementById('clear2');
                            cancelButton2.addEventListener('click', function (event) {
                                signaturePad2.clear();
                            });

                            function onFormSubmit()
                            {
                                document.getElementById('sig').value = signaturePad.toDataURL();
                                document.getElementById('sig1').value = signaturePad1.toDataURL();
                                document.getElementById('sig2').value = signaturePad2.toDataURL();
                            }
                        </script>

                        <button type="submit" class="btn btn-primary btn-block">Update</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection