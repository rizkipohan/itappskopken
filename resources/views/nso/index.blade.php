@extends('nso.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">NSO Document List</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="">
    
                <div class="form-inline my-3 justify-content-center">
                    <a name="" id="" class="btn btn-success mx-2" href="{{url('/nso/create')}}" role="button">Create New NSO PDF</a>

                    {{$nso->links()}}
                </div>
                
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped ">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Store Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($nso)==0)
                                   <tr class="text-center"> <td colspan="100">No Data</td> </tr>
                                @else
                                    @foreach ($nso as $nso)
                                    <tr class="text-center">
                                        <td>{{$nso->id}}</td>
                                        <td>{{$nso->store['store_name']}}</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ url('nso/edit/'.$nso->id) }}">Edit / Details</a> 
                                            <a class="btn btn-danger" href="{{url('nso/print/'.$nso->id)}}">Print PDF</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
