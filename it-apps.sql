-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2020 at 08:21 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `it-apps`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'unknown', '2020-03-28 08:23:37', '2020-03-28 08:23:37'),
(2, 'IT', '2020-03-28 08:23:37', '2020-03-28 08:23:37'),
(3, 'Marketing', '2020-03-28 08:23:37', '2020-03-28 08:23:37'),
(4, 'Finance/Accounting', '2020-03-28 08:23:37', '2020-03-28 08:23:37'),
(5, 'HR', '2020-05-27 16:00:00', '2020-05-28 16:00:00'),
(6, 'SCM', '2020-05-29 09:00:00', '2020-05-28 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mime_type` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_path` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `folder_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `department_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `item_in`
--

CREATE TABLE `item_in` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_id` bigint(20) UNSIGNED DEFAULT NULL,
  `vendor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `po_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'NULL',
  `pr_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'NULL',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `department_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `item_out`
--

CREATE TABLE `item_out` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `store_id` bigint(20) UNSIGNED DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_id` bigint(20) UNSIGNED DEFAULT NULL,
  `employee_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Table structure for table `nso`
--

CREATE TABLE `nso` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `store_id` bigint(20) UNSIGNED DEFAULT NULL,
  `json` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `store_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_date` date DEFAULT NULL,
  `store_address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connection_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mikrotik_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_local` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_wan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_gateway_wan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_dns` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_vpn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `ticketit`
--

CREATE TABLE `ticketit` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `html` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_site` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `priority_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `agent_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `completed_at` timestamp NULL DEFAULT NULL,
  `department_id` bigint(20) UNSIGNED DEFAULT 1,
  `subcategory_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Table structure for table `ticketit_audits`
--

CREATE TABLE `ticketit_audits` (
  `id` int(10) UNSIGNED NOT NULL,
  `operation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ticket_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticketit_categories`
--

CREATE TABLE `ticketit_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` bigint(20) UNSIGNED DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `ticketit_categories_users`
--

CREATE TABLE `ticketit_categories_users` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `ticketit_comments`
--

CREATE TABLE `ticketit_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `html` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ticket_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `ticketit_priorities`
--

CREATE TABLE `ticketit_priorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `ticketit_settings`
--

CREATE TABLE `ticketit_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `default` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Table structure for table `ticketit_statuses`
--

CREATE TABLE `ticketit_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Table structure for table `ticketit_subcategories`
--

CREATE TABLE `ticketit_subcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT 3,
  `department_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `ticketit_admin` tinyint(1) NOT NULL DEFAULT 0,
  `ticketit_agent` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_department_name_unique` (`name`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_folder_id_foreign` (`folder_id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_department_id_foreign` (`department_id`);

--
-- Indexes for table `item_in`
--
ALTER TABLE `item_in`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_in_item_id_foreign` (`item_id`),
  ADD KEY `item_in_store_id_foreign` (`store_id`),
  ADD KEY `item_in_department_id_foreign` (`department_id`);

--
-- Indexes for table `item_out`
--
ALTER TABLE `item_out`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_out_item_id_foreign` (`item_id`),
  ADD KEY `item_out_store_id_foreign` (`store_id`),
  ADD KEY `item_out_department_id_foreign` (`department_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nso`
--
ALTER TABLE `nso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nso_store_id_foreign` (`store_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `stores_store_code_unique` (`store_code`);

--
-- Indexes for table `ticketit`
--
ALTER TABLE `ticketit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticketit_subject_index` (`subject`),
  ADD KEY `ticketit_status_id_index` (`status_id`),
  ADD KEY `ticketit_priority_id_index` (`priority_id`),
  ADD KEY `ticketit_user_id_index` (`user_id`),
  ADD KEY `ticketit_agent_id_index` (`agent_id`),
  ADD KEY `ticketit_category_id_index` (`category_id`),
  ADD KEY `ticketit_completed_at_index` (`completed_at`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `ticketit_subcategory_id_foreign` (`subcategory_id`);

--
-- Indexes for table `ticketit_audits`
--
ALTER TABLE `ticketit_audits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticketit_categories`
--
ALTER TABLE `ticketit_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `ticketit_comments`
--
ALTER TABLE `ticketit_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticketit_comments_user_id_index` (`user_id`),
  ADD KEY `ticketit_comments_ticket_id_index` (`ticket_id`);

--
-- Indexes for table `ticketit_priorities`
--
ALTER TABLE `ticketit_priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticketit_settings`
--
ALTER TABLE `ticketit_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ticketit_settings_slug_unique` (`slug`),
  ADD UNIQUE KEY `ticketit_settings_lang_unique` (`lang`),
  ADD KEY `ticketit_settings_lang_index` (`lang`),
  ADD KEY `ticketit_settings_slug_index` (`slug`);

--
-- Indexes for table `ticketit_statuses`
--
ALTER TABLE `ticketit_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticketit_subcategories`
--
ALTER TABLE `ticketit_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_department_id_foreign` (`department_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `item_in`
--
ALTER TABLE `item_in`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `item_out`
--
ALTER TABLE `item_out`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3037;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `nso`
--
ALTER TABLE `nso`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=454;

--
-- AUTO_INCREMENT for table `ticketit`
--
ALTER TABLE `ticketit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=279;

--
-- AUTO_INCREMENT for table `ticketit_audits`
--
ALTER TABLE `ticketit_audits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticketit_categories`
--
ALTER TABLE `ticketit_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ticketit_comments`
--
ALTER TABLE `ticketit_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ticketit_priorities`
--
ALTER TABLE `ticketit_priorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ticketit_settings`
--
ALTER TABLE `ticketit_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `ticketit_statuses`
--
ALTER TABLE `ticketit_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ticketit_subcategories`
--
ALTER TABLE `ticketit_subcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_folder_id_foreign` FOREIGN KEY (`folder_id`) REFERENCES `folders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `item_in`
--
ALTER TABLE `item_in`
  ADD CONSTRAINT `item_in_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `item_in_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_in_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `item_out`
--
ALTER TABLE `item_out`
  ADD CONSTRAINT `item_out_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `item_out_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_out_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nso`
--
ALTER TABLE `nso`
  ADD CONSTRAINT `nso_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ticketit`
--
ALTER TABLE `ticketit`
  ADD CONSTRAINT `ticketit_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ticketit_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `ticketit_subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ticketit_categories`
--
ALTER TABLE `ticketit_categories`
  ADD CONSTRAINT `ticketit_categories_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE `bast_nso` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `store_id` bigint(20) UNSIGNED DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filename`  varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `bast_nso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bast_nso_store_id_foreign` (`store_id`);

ALTER TABLE `bast_nso`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `bast_nso`
  ADD CONSTRAINT `bast_nso_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `bast_nso` ADD `created_at` TIMESTAMP NULL DEFAULT NULL AFTER `filename`, ADD `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER `created_at`;

CREATE TABLE `request_item` (
  `id` bigint(20) NOT NULL,
  `store_id` bigint(20) UNSIGNED DEFAULT NULL,
  `items` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`items`)),
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `approve_status` enum('0','1','2','') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `store_properties` (
  `id` bigint(20) NOT NULL,
  `store_id` bigint(20) UNSIGNED DEFAULT NULL,
  `open_date` date DEFAULT NULL,
  `pos` varchar(200) DEFAULT NULL,
  `pos_wifi` varchar(200) DEFAULT NULL,
  `wifi_extended_cable` varchar(200) DEFAULT NULL,
  `data_product_promo` varchar(200) DEFAULT NULL,
  `employee_register` varchar(200) DEFAULT NULL,
  `employee_training` varchar(200) DEFAULT NULL,
  `pos_trainer` varchar(200) DEFAULT NULL,
  `printer_label` varchar(200) DEFAULT NULL,
  `usb_lcd_content` varchar(200) DEFAULT NULL,
  `tablet` varchar(200) DEFAULT NULL,
  `apps_id` varchar(200) DEFAULT NULL,
  `apps_pin` varchar(200) DEFAULT NULL,
  `t_hub` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `laptop` varchar(200) DEFAULT NULL,
  `internet` varchar(200) DEFAULT NULL,
  `isp` varchar(200) DEFAULT NULL,
  `sid` varchar(200) DEFAULT NULL,
  `ip_public` varchar(200) DEFAULT NULL,
  `ip_reserved` varchar(200) DEFAULT NULL,
  `cctv_cam_qty` int(11) DEFAULT NULL,
  `cctv_recording` varchar(200) DEFAULT NULL,
  `cctv_cloud_id` varchar(200) DEFAULT NULL,
  `cctv_streaming` varchar(200) DEFAULT NULL,
  `cctv_regional` varchar(200) DEFAULT NULL,
  `code_creating_order` varchar(200) DEFAULT NULL,
  `create_offline_order` varchar(200) DEFAULT NULL,
  `check_order_appear_in_apps` varchar(200) DEFAULT NULL,
  `scan_qr_code_create_order` varchar(200) DEFAULT NULL,
  `pic_name` varchar(200) NOT NULL,
  `pic_phone` varchar(200) DEFAULT NULL,
  `pic_email` varchar(200) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `request_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `store_id` (`store_id`);

ALTER TABLE `request_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `request_item`
  ADD CONSTRAINT `request_item_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `request_item_ibfk_2` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

ALTER TABLE `users` ADD `it_admin` INT(10) NULL AFTER `ticketit_agent`;
ALTER TABLE `item_out` ADD `request_item_id` BIGINT NULL AFTER `employee_name`;
ALTER TABLE `item_out` ADD FOREIGN KEY (`request_item_id`) REFERENCES `request_item`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;



ALTER TABLE `store_properties`  ADD `code_creating_order` VARCHAR(200) NULL  AFTER `registered_cloud`,  ADD `create_offline_order` VARCHAR(200) NULL  AFTER `code_creating_order`,  ADD `check_order_appear_in_apps` VARCHAR(200) NULL  AFTER `create_offline_order`,  ADD `scan_qr_code_creat_order` VARCHAR(200) NULL  AFTER `check_order_appear_in_apps`;

/*
=========================================================================================================================================
=========================================================================================================================================
=========================================================================================================================================
=========================================================================================================================================
=========================================================================================================================================
=========================================================================================================================================
=========================================================================================================================================
=========================================================================================================================================
=========================================================================================================================================
=========================================================================================================================================
=========================================================================================================================================
*/

INSERT INTO `stores` (`id`, `store_name`, `store_code`, `store_status`, `open_date`, `store_address`, `store_email`, `store_phone`, `tax_number`, `connection_status`, `provider`, `sid`, `mikrotik_status`, `ip_local`, `ip_wan`, `ip_gateway_wan`, `ip_dns`, `ip_vpn`, `created_at`, `updated_at`) VALUES
(1, 'Menara Standart Chartered', 'MSC', 'Open', '2017-08-22', 'Menara Standard Chartered, Lantai Ground Floor, Jl. Prof. DR. Satrio No.164 Setiabudi Kota Jakarta Selatan, 12930', 'jkt.msc@kopikenangan.com', '0817 9875 826', NULL, 'Online', 'NEXT One', NULL, NULL, '10.1.35.0/24', NULL, NULL, NULL, '172.16.0.35', NULL, '2020-05-04 12:49:49'),
(2, 'Setiabudi One', 'SONE', 'OPEN', '2018-03-11', 'Setiabudi One, Lantai 1 Unit. B211, Jl. H. R. Rasuna Said, Setiabudi Kota Jakarta Selatan, 12920', 'jkt.sone@kopikenangan.com', '0817 9875 827', NULL, 'Online', 'Telkom', '924955114', 'Sudah dikirim', '10.10.1.0/24', '36.92.111.113/24', '36.92.111.112', '118.98.44.10/20', NULL, NULL, NULL),
(3, 'Kota Kasablanka', 'KOKAS', 'OPEN', '2018-04-02', 'Mall Kota Kasablanka, Lantai GF, Food Society Kota Kasablanka JL. Menteng Pulo, Tebet, Kota Jakarta Selatan,12870', 'jkt.kokas@kopikenangan.com', '0817 9875 828', NULL, 'Online', 'Envision', NULL, 'Sudah dikirim', '10.10.2.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(4, 'Wisma Mulia', 'WISMUL', 'Open', '2018-05-14', 'Wisma Mulia, Lantai Lantai Ground Floor, Jl. Gatot Subroto, Mampang Prapatan, Kota Jakarta Selatan, 12710', 'jkt.wismul@kopikenangan.com', '0817 9875 829', NULL, 'Online', NULL, NULL, NULL, '10.10.3.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, '2020-06-14 05:11:36'),
(5, 'RDTX Tower', 'RDTX', 'OPEN', '2018-07-06', 'RDTX Tower, Lantai Ground floor, Jl. Kuningan Barat Raya, Jakarta Selatan, Jakarta, 12710', 'jkt.rdtx@kopikenangan.com', '0817 9875 830', NULL, 'Online', 'Moratel', '3085610000000000', 'Sudah dikirim', '10.10.4.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(6, 'Tempo Scan Tower', 'TST', 'OPEN', '2018-08-27', 'Tempo Scan Tower, Lantai Basement Jl. H. R. Rasuna Said No.Kav3-4 Setia Budi, Jakarta Selatan, 12950', 'jkt.tst@kopikenangan.com', '0817 9875 832', NULL, 'Online', 'Telkom', '926581798', 'Sudah dikirim', '10.10.5.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(7, 'Menara Karya', 'KARYA', 'OPEN', '2018-09-03', 'Menara Karya, Lantai Basement Unit.F Jl. H.R Rasuna Said, Kuningan Timur, Jakarta Selatan, 12950', 'jkt.karya@kopikenangan.com', '0817 9875 833', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.10.8.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(8, 'Equity Tower', 'EQUITY', 'OPEN', '2018-09-17', 'Equity Tower, Lantai Ground floor, Jl. Jend. Sudirman No.Kav. 52-53 Senayan, Kebayoran Baru, Jekarta Selatan, 12180', 'jkt.equity@kopikenangan.com', '0817 9875 835', NULL, 'Online', 'Arthatel', '190971000000', 'Sudah dikirim', '10.10.7.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(9, 'Plaza Semanggi', 'PLANGI', 'OPEN', '2018-09-27', 'Plaza Semanggi, Lantai 3, Jl. Jend. Sudirman Karet Semanggi, Kota Jakarta Selatan, 12930', 'jkt.plangi@kopikenangan.com', '0817 9875 836', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.10.9.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(10, 'Mangkuluhur City', 'MK', 'OPEN', '2018-10-01', 'Mangkuluhur City (Hana Bank) Jl. Jend. Gatot Subroto Kav. 1-3, Karet Semanggi,Kota Jakarta Selatan, 12930', 'jkt.mk@kopikenangan.com', '0817 9875 838', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.10.12.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(11, 'Grand Indonesia', 'GI', 'OPEN', '2018-10-03', 'East Mall Grand Indonesia, Lantai Upper Ground Floor, Jl. M.H. Thamrin No.1 Menteng, Kota Jakarta Pusat, 10310', 'jkt.gi@kopikenangan.com', '0817 9875 839', NULL, 'On Activation', 'Indihome', NULL, 'Sudah dikirim', '10.1.36.0/24', NULL, NULL, NULL, '172.16.0.36', NULL, NULL),
(12, 'Plaza Oleos', 'OLEOS', 'OPEN', '2018-10-08', 'Plaza Oleos, D\'Hana Lounge , Lantai Ground Floor, Jl TB. Simatupang Kav. 53, Kebagusan, Kota Jakarta Selatan, 12520', 'jkt.oleos@kopikenangan.com', '0817 9875 840', NULL, 'Online', 'Telkom', '966207730', 'Sudah dikirim', '10.10.11.0/24', '36.92.144.81/24', '36.92.144.80', '118.98.44.10/20', NULL, NULL, NULL),
(13, 'Pesona Square Depok', 'PESONA', 'OPEN', '2018-10-12', 'Pesona Square Mall, Lantai 2, Food Avenue, Jl. Ir. H. Juanda No. 99, Sukmajaya, Depok, 16418', 'jkt.pesona@kopikenangan.com', '0817 9875 841', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.37.0/24', NULL, NULL, NULL, '172.16.0.37', NULL, NULL),
(14, 'Capital Place', 'CAPITAL', 'OPEN', '2018-10-15', 'Capital Place, Lantai Ground Floor, Jl Jenderal Gatot Subroto Kav. 18, Gatot Subroto, Jakarta Selatan, 12710', 'jkt.capital@kopikenangan.com', '0817 9875 845', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.10.18.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(15, 'Skyline Building', 'SKYLINE', 'OPEN', '2018-10-22', 'Skyline Building, Lantai 1, Jl Mh. Thamrin No. 9, Jakarta Pusat, 12920', 'jkt.skyline@kopikenangan.com', '0817 9875 847', NULL, 'Online', 'Telkom', '926543980', 'Sudah dikirim', '10.10.14.0/24', '36.92.120.143/24', '36.92.120.142', '118.98.44.10/20', NULL, NULL, NULL),
(16, 'Graha CIMB Niaga', 'CIMB', 'OPEN', '2018-11-15', 'Graha CIMB Niaga, Lantai Ground Floor, SCBD, Jl. Jend. Sudirman Kav. 58, Karet Semanggi, Jakarta Selatan, 12930', 'jkt.cimb@kopikenangan.com', '0817 9875 849', NULL, 'Online', 'Bali Fiber', '41200050109', 'Sudah dikirim', '10.10.15.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(17, 'Electronic City', 'EC', 'OPEN', '2018-11-19', 'Electronic City Building, Lantai 2, SCBD, Jl. Jend Sudirman Kav. 52-53 Karet Semanggi, Jakarta Selatan, 12190', 'jkt.ec@kopikenangan.com', '0817 9875 862', NULL, 'Online', 'Arthatel', '190971000000', 'Sudah dikirim', '10.10.16.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(18, 'Mayapada Hospital', 'mayapadahos', 'OPEN', '2018-11-22', 'Mayapada Hospital, Lantai Ground Floor, Jl. Lebak Bulus I kav. 29,Cilandak Barat, Jakarta Selatan, 12440', 'jkt.mayapadahos@kopikenangan.com', '0817 9875 864', NULL, 'Online', 'Telkom', '923888943', 'Sudah dikirim', '10.10.17.0/24', '36.92.80.199/24', '36.92.80.198', '118.98.44.10/20', NULL, NULL, NULL),
(19, 'Lippo Mall Puri', 'LIPPO-PURI', 'OPEN', '2018-11-23', 'Lippo Mall Puri, LG, Jl. Puri Indah Boulevard Blok U No. 1, Puri Indah, Jakarta Barat, 11610', 'jkt.lippo-puri@kopikenangan.com', '0817 9875 866', NULL, 'Online', 'LinkNET', NULL, 'Sudah dikirim', '10.1.38.0/24', 'DHCP Client', NULL, NULL, '172.16.0.38', NULL, NULL),
(20, 'Pejaten Village', 'PENVIL', 'OPEN', '2018-11-29', 'Pejaten Village, Lantai Ground Floor, Jl. Warung Jati Barat No. 39, Jati Padang, Jakarta Selatan, 12540', 'jkt.penvil@kopikenangan.com', '0817 9875 867', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.10.22.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(21, 'Kyai Maja', 'KYAI.MAJA', 'OPEN', '2018-11-30', 'Jl. Kyai Maja No. 29, Rt. 010 Rw. 007 Kramat Pela, Kebayoran, Jakarta Selatan 12130', 'jkt.kyai.maja@kopikenangan.com', '0817 9875 869', NULL, 'Online', 'Telkom', '922723421', 'Sudah dikirim', '10.10.20.0/24', '36.92.67.197/24', '36.92.67.196', '118.98.44.10/20', NULL, NULL, NULL),
(22, 'Lotte Avenue', 'LOTTE', 'OPEN', '2018-12-03', 'Lotte Shopping Avenue, Food Avenue Lantai 4, Jl. Prof DR. Satrio No. 3-5,Karet Kuningan, Jakarta Selatan, 12940', 'jkt.lotte@kopikenangan.com', '0817 9875 870', NULL, 'Online', 'Telkom/Next One', '924884229', 'Sudah dikirim', '10.10.21.0/24', '36.92.111.35/24', '36.92.111.34', '118.98.44.10/20', NULL, NULL, NULL),
(23, 'Plaza Atrium', 'ATRIUM', 'OPEN', '2018-12-12', 'Plaza Atrium, Lantai Ground Floor, Jl. Senen Raya, Senen, Jakarta Pusat, 10410', 'jkt.atrium@kopikenangan.com', '0817 9875 872', NULL, 'Online', 'Indonet', '54987086', 'Sudah dikirim', '10.10.23.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(24, 'Summarecon Mall Serpong', 'SMS', 'OPEN', '2018-12-18', 'Sumarecon Mall Serpong, Lantai GF, Jl. Boulevard Raya Gading Serpong, Kelapa Dua, Tangerang,15810', 'jkt.sms@kopikenangan.com', '0817 9875 873', NULL, 'Online', 'Telkom', '924644260', 'Sudah dikirim', '10.10.24.0/24', '36.92.87.179/24', '36.92.87.178', '118.98.44.10/20', NULL, NULL, NULL),
(25, 'Kalibata City', 'KALCIT', 'OPEN', '2018-12-21', 'Apartement Kalibata City, Lantai Ground Floor, Jl. Raya Kalibata No.1, RT.9/RW.4, Pancoran, Jakarta Selatan,12750', 'jkt.kalcit@kopikenangan.com', '0817 9875 874', NULL, 'Online', 'Telkom', '922724307', 'Sudah dikirim', '10.10.25.0/24', '36.92.67.201/24', '36.92.67.200', '118.98.44.10/20', NULL, NULL, NULL),
(26, 'Mall Ambasador', 'AMBAS', 'OPEN', '2019-01-03', 'Mall Ambasador, Lantai 4, Jl. Prof. DR. Satrio No.65,Setiabudi, Kota Jakarta Selatan, 12940', 'jkt.ambas@kopikenangan.com', '0817 9875 879', NULL, 'Online', 'Telkom', '923936229', 'Sudah dikirim', '10.10.26.0/24', '36.92.81.89/24', '36.92.81.88', '118.98.44.10/20', NULL, NULL, NULL),
(27, 'Citywalk Sudirman', 'CIWALK1', 'OPEN', '2019-01-08', 'Citywalk Sudirman, Lantai 2, Jl. K.H. Mas Mansyur, Tanah Abang, Kota Jakarta Pusat, 10220', 'jkt.ciwalk1@kopikenangan.com', '0817 9875 882', NULL, 'Online', 'Telkom/Next One', '923715201', 'Sudah dikirim', '10.10.27.0/24', '36.92.80.201/24', '36.92.80.200', '118.98.44.10/20', NULL, NULL, NULL),
(28, 'Sahid Sudirman', 'SAHID', 'OPEN', '2019-01-10', 'Sahid City Kav. 86, Lantai Dasar GF, Jl. Jend. Sudirman, Tanah Abang, Jakarta Pusat,10220', 'jkt.sahid@kopikenangan.com', '0817 9875 884', NULL, 'Online', 'Telkom', '923719430', 'Sudah dikirim', '10.10.28.0/24', '36.92.80.161/24', '36.92.80.160', '118.98.44.10/20', NULL, NULL, NULL),
(29, 'Botani Square Mall Bogor', 'BOTANI', 'OPEN', '2019-01-15', 'Botani Square Mall, Eatery Lantai 2, Jl. Raya Pajajaran No.69-71,Bogor Tengah, Kota Bogor ,16129', 'bgr.botani@kopikenangan.com', '0817 9875 885', NULL, 'On Provisioning', 'NEXT One', NULL, 'Sudah dikirim', '10.1.124.0/24', NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'Cibubur Junction', 'CJ', 'OPEN', '2019-01-16', 'Cibubur Junction, Lantai Lower Ground Floor, Jl. Jambore No.14,Cibubur, Jakarta Timur, 13720', 'jkt.cj@kopikenangan.com', '0817 9875 887', NULL, 'Online', 'Telkom', '922408820', 'Sudah dikirim', '10.10.30.0/24', '36.92.67.135/24', '36.92.67.134', '118.98.44.10/20', NULL, NULL, NULL),
(31, 'Pasaraya Blok M', 'PASARAYA', 'OPEN', '2019-01-16', 'Pasaraya Blok M, Lantai Ground Floor, Jl. Iskandarsyah II, Kebayoran Baru, Jakarta Selatan, 12160', 'jkt.pasaraya@kopikenangan.com', '0817 9875 890', NULL, 'Online', 'Telkom', '924426386', 'Sudah dikirim', '10.10.31.0/24', '36.92.81.213/24', '36.92.81.212', '118.98.44.10/20', NULL, NULL, NULL),
(32, 'Summitmas Tower', 'SUMMIT', 'OPEN', '2019-01-28', 'Summitmas Tower, Lantai Lower Ground, Jl. Jend. Sudirman No.52-53, SenayanJakarta Selatan, 12190', 'jkt.summit@kopikenangan.com', '0817 9875 891', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.39.0/24', NULL, NULL, NULL, '172.16.0.39', NULL, NULL),
(33, 'Plaza Festival', 'PLAFES', 'OPEN', '2019-01-29', 'Plaza Festival, Lantai Uper Ground, Komplek Rasuna Epicentrum, Jl. H. R. Rasuna Said Kav. C22, Karet Kuningan, Jakarta Selatan, 12940', 'jkt.plafes@kopikenangan.com', '0817 9875 893', NULL, 'Online', 'Next One', NULL, 'Sudah dikirim', '10.10.33.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(34, 'Mall Kelapa Gading 3', 'MKG3', 'OPEN', '2019-01-31', 'Mall Kelapa Gading 3, Lantai Ground Floor, Jl. Bulevar Kelapa Gading, Klp. Gading, Jakarta Utara, 14240', 'jkt.mkg3@kopikenangan.com', '0817 9875 894', NULL, 'Online', 'Telkom', '924763416', 'Sudah dikirim', '10.10.34.0/24', '36.92.87.223/24', '36.92.87.222', '118.98.44.10/20', NULL, NULL, NULL),
(35, 'Thamrin Residence', 'THAMRIN', 'OPEN', '2019-02-04', 'Thamrin Residence Office Park, Block RC No. 02, Jl. Thamrin Residence, Tanah abang Jakarta Pusat, 10220', 'jkt.thamrin@kopikenangan.com', '0817 9875 896', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.40.0/24', NULL, NULL, NULL, '172.16.0.40', NULL, NULL),
(36, 'Wisma KEIAI', 'KEIAI', 'OPEN', '2019-02-07', 'Wisma KEIAI, Lantai Ground Floor, Jl. Jend. Sudirman, Karet Tengsin, Kota Jakarta Pusat, 10250', 'jkt.keiai@kopikenangan.com', '0817 9875 897', NULL, 'Online', 'Bali Fiber', '41200050138', 'Sudah dikirim', '10.10.36.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(37, 'Mall @BASSURA CITY', 'BASSURA', 'OPEN', '2019-02-09', 'Mall @Basura, Lantai GF, Jl. Basuki Rahmat No.1A, Cipinang Besar Selatan, Kota Jakarta Timur, 13410', 'jkt.bassura@kopikenangan.com', '0817 9875 899', NULL, 'Online', 'Telkom', '967394457', 'Sudah dikirim', '10.10.37.0/24', '36.92.147.161/24', '36.92.147.160', '180.131.144.144/145.145', NULL, NULL, NULL),
(38, 'Sopo Del Tower', 'SOPODEL', 'OPEN', '2019-02-13', 'Sopo Del Tower, Lantai Ground Floor, Jl. Mega Kuningan Barat III, Kuningan Timur, Jakarta Selatan, 12950', 'jkt.sopodel@kopikenangan.com', '0817 9875 900', NULL, 'On Activation', 'NEXT One', NULL, 'Sudah dikirim', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'Tempo Pavilion 1', 'PAVI', 'OPEN', '2019-02-19', 'Tempo Pavilion 1, Lantai Ground Floor, Jl. H. R. Rasuna Said No.11 Kuningan Timur, Jakarta Selatan, 12950', 'jkt.pavi@kopikenangan.com', '0817 9875 903', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.10.39.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(40, 'Transmart Juanda Bekasi', 'TMJ', 'OPEN', '2019-03-01', 'Transmart Juanda Bekasi, Lantai Ground Floor, Jl. Ir. H. Juanda No.180, Bekasi Timur, Jawa Barat, 17113', 'jkt.tmj@kopikenangan.com', '0817 9875 904', NULL, 'Online', 'Telkom', NULL, 'Sudah dikirim', '10.10.40.0/24', '36.92.118.53/24', '36.92.118.52', '118.98.44.10/20', NULL, NULL, NULL),
(41, 'Pondok Indah Mall 2', 'PIM2', 'OPEN', '2019-03-06', 'Pondok Indah Mall 2, Lantai 3, Jl. Metro Pondok Indah No.Kav. IV, Kebayoran Lama, Kota Jakarta Selatan, 12310', 'jkt.pim2@kopikenangan.com', '0817 9875 905', NULL, 'Online', 'Biznet', '123100000000', 'Sudah dikirim', '10.10.41.0/24', NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'Food Centrum Sunter', 'FC.SUNTER', 'OPEN', '2019-03-11', 'Food Centrum, Lantai 1, Ruko Puri Mutiara, Jl. Griya Utama Blok BA-BB-BC, Tj. Priok, Kota Jkt Utara, 14350', 'jkt.fc.sunter@kopikenangan.com', '0817 9875 910', NULL, 'Online', 'Telkom', '924864843', 'Sudah dikirim', '10.10.42.0/24', '36.92.111.5/24', '36.92.111.4', '118.98.44.10/20', NULL, NULL, NULL),
(43, 'Giant Bintaro', 'Giant.Bintaro', 'OPEN', '2019-03-12', 'Giant CBD Tanggerang, Lantai GF, Sektor 7, Jl. Boulevard Bintaro Jaya Blok B7, Pondok Jaya Tangerang,15224', 'tgr.giant.bintaro@kopikenangan.com', '0817 9875 911', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.1.41.0/24', NULL, NULL, NULL, '172.16.0.41', NULL, NULL),
(44, 'Pacific Place', 'PP', 'OPEN', '2019-03-15', 'Pacific Place, Lantai Lower Ground, Jl. Jend. Sudirman No.52-53, Kby. Baru, Kota Jakarta Selatan, 12190', 'jkt.pp@kopikenangan.com', '0817 9875 913', NULL, 'Online', 'Arthatel', '190971000000', 'Sudah dikirim', '10.10.44.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(45, 'Green Pramuka Square', 'PRAMUKA', 'OPEN', '2019-03-22', 'Green Pramuka Square, Lantai Ground Floor, Jl. Rawa Jaya No.49, Cemp. Putih, Kota Jakarta Pusat, 10570', 'jkt.pramuka@kopikenangan.com', '0817 9875 919', NULL, 'Online', 'Telkom', NULL, 'Sudah dikirim', '10.10.45.0/24', '36.92.200.83/24', '36.92.200.82', '118.98.44.10/20', NULL, NULL, NULL),
(46, 'Mall Lippo CIkarang', 'LIPPO.CIKA', 'OPEN', '2019-03-25', 'Lippo Cikarang, Lantai 1, Jl. MH. Thamrin, Cibatu, Cikarang Selatan, Jawa Barat, 17550', 'bks.lippo.cikarang@kopikenangan.com', '0817 9875 920', NULL, 'Online', 'Telkom', '1109721740', 'Sudah dikirim', '10.10.46.0/24', '36.92.161.79/24', '36.92.161.78', '180.131.144.144/145.145', NULL, NULL, NULL),
(47, 'The Breeze', 'BREEZE', 'OPEN', '2019-03-27', 'The Breeze, Lake Level, Jl. BSD Green Office Park, BSD City, Cisauk, Tangerang, 15345', 'tgr.breeze@kopikenangan.com', '0817 9875 922', NULL, 'Online', 'Bali Fiber', NULL, 'Sudah dikirim', '10.10.47.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(48, 'Revo Town', 'REVO', 'OPEN', '2019-03-28', 'Revo Town Bekasi, Lantai Ground Floor, Jl. Ahmad Yani, Bekasi Selatan, 17148', 'bks.revo@kopikenangan.com', '0817 9875 930', NULL, 'On Activation', 'NEXT One', NULL, 'Sudah dikirim', '10.1.42.0/24', NULL, NULL, NULL, '172.16.0.42', NULL, NULL),
(49, 'Baywalk Mall Pluit', 'BAYWALK', 'OPEN', '2019-03-30', 'Baywalk, Lantai 2, Jl. Pluit Karang Ayu 1, Penjaringan, Jakarta Utara, 14450', 'jkt.baywalk@kopikenangan.com', '0817 9875 952', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.43.0/24', NULL, NULL, NULL, '172.16.0.43', NULL, NULL),
(50, 'DMall Depok', 'DMALL', 'OPEN', '2019-03-30', 'Dmall Depok, Lantai Ground Floor, Jl. Margonda Raya Kav. 88, Kemiri Muka, Beji, Depok, 16423', 'dpk.dmall@kopikenangan.com', '0817 9875 956', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.46.0/24', NULL, NULL, NULL, '172.16.0.46', NULL, NULL),
(51, 'Menara Jamsostek', 'Jamsostek', 'OPEN', '2019-04-01', 'Gedung Parkir Menara Jamsostek, Lantai 1, Jl. Jendral Gatot Subroto No.Kav.38, Mampang Prapatan, Jakarta Selatan, 12710', 'jkt.jamsostek@kopikenangan.com', '0817 9875 957', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.45.0/24', NULL, NULL, NULL, '172.16.0.45', NULL, NULL),
(52, 'Lotte Mart Bintaro', 'LOTTEB', 'OPEN', '2019-04-04', 'Lotte Mart Bintaro, Lantai Ground Floor, JL M.H. Thamrin CBD Area Kav. Blok B7 01-06, Tangerang, 15224', 'jkt.lotteb@kopikenangan.com', '0817 9875 958', NULL, 'Online', 'Telkom', '957923646', 'Sudah dikirim', NULL, '36.92.194.209/24', '36.92.194.208', '118.98.44.10/20', NULL, NULL, NULL),
(53, 'Lotte Mart Fatmawati', 'Lotte.Fatmawati', 'OPEN', '2019-04-04', 'Lotte Mart Fatmawati, Lantai Ground floor, Jl. RS Fatmawati No.15, Komp. Golden Fatmawati, Cilandak, Jakarta Selatan, 12420', 'jkt.lotte.fatmawati@kopikenangan.com', '0817 9875 961', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.44.0/24', NULL, NULL, NULL, '172.16.0.44', NULL, NULL),
(54, 'Trans Studio Mall Cibubur', 'TSMC', 'OPEN', '2019-04-05', 'Trans Studio Mall Cibubur, Lantai Lower Ground, Jl. Alternatif Cibubur No.230, Cimanggis, Depok, 16454', 'jkt.tsmc@kopikenangan.com', '0817 9875 962', NULL, 'On Provisioning', 'NEXT One', NULL, NULL, '10.1.47.0/24', NULL, NULL, NULL, '172.16.0.47', NULL, NULL),
(55, 'City Plaza Jatinegara', 'Cityplaza', 'OPEN', '2019-04-06', 'Cityplaza Jatinegara, Lantai 1, Jl. Matraman Raya, Jatinegara, Jakarta Timur, 13310', 'jkt.cityplaza@kopikenangan.com', '0817 9875 965', NULL, 'Online', 'Bali Fiber', '41200050111', 'Sudah dikirim', '10.10.55.0/24', NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'Gandaria City', 'Gancit', 'OPEN', '2019-04-07', 'Gandaria City Mall, Lantai Upper Ground, Jl.Kebayoran Lama Kby. Lama, Kota Jakarta Selatan, 12240', 'jkt.gancit@kopikenangan.com', '0817 9875 968', NULL, 'Online', 'Envision', NULL, 'Sudah dikirim', '10.10.56.1', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(57, 'Surabaya Manyar', 'MANYAR.SBY', 'OPEN', '2019-04-08', 'Jalan Manyar Kertoarjo No. 26 , Surabaya, Jawa Timur, 60284', 'manyar.sby@kopikenangan.com', '0817 9875 969', NULL, 'Online', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'Kelapa Gading Barat', 'KGB', 'OPEN', '2019-04-08', 'Kelapa Gading Barat (HANA), Jl. Boulevard Barat Blok LC6 No. 30-32, Kelapa Gading, Jakarta Utara, 14120', 'jkt.kgb@kopikenangan.com', '0817 9875 972', NULL, 'On Provisioning', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Lippo Plaza Kramat Jati', 'LIPPO.KJ', 'OPEN', '2019-04-16', 'Lippo Plaza Kramat Jati, Ground Floor, Jl. Raya Bogor KM.19, RT.14/RW.6, Kramat Jati, Jakarta Timur, 13510', 'jkt.lippo.kj@kopikenangan.com', '0817 9875 978', NULL, 'Online', 'Telkom', '923836064', 'Sudah dikirim', '10.10.59.0/24', '36.92.80.227/24', '36.92.80.226', '118.98.44.10/20', NULL, NULL, NULL),
(60, 'Aeon Mall Jakarta Garden City', 'AEON.JGC', 'OPEN', '2019-04-18', 'AEON MALL Jakarta Garden City, Lantai 1, Jl. Jakarta Garden City, Cakung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta, 13910', 'jkt.aeon.jgc@kopikenangan.com', '0817 9875 980', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.49.0/24', NULL, NULL, NULL, '172.16.0.49', NULL, NULL),
(61, 'Tunjungan Plaza 3', 'TP3.SBY', 'OPEN', '2019-04-25', 'Tunjungan Plaza 3, Lantai 5, Jl. Basuki Rahmat No.107, Tegalsari, Surabaya, 60261', 'tp3.sby@kopikenangan.com', '0817 9876 022', NULL, 'Online', 'NEXT One', NULL, 'Sudah DIkirim', '10.10.62.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(62, 'Pusat Grosir Cililitan', 'PGC', 'OPEN', '2019-04-25', 'Pusat Grosir Cililitan, Lantai Lower Ground, Jl. Dewi Sartika, Cililitan, Kramatjati, Kota Jakarta Timur, 13640', 'jkt.pgc@kopikenangan.com', '0817 9876 023', NULL, 'On Activation', 'NEXT One', NULL, 'Sudah DIkirim', '10.1.50.0/24', NULL, NULL, NULL, '172.16.0.50', NULL, NULL),
(63, 'Central Park', 'CP', 'OPEN', '2019-04-30', 'Central Park, Lower Ground Floor, Letjen S. Parman Street No.28, RT.12/RW.6, Tanjung Duren Selatan, Grogol petamburan, Jakarta Barat, 11470', 'jkt.cp@kopikenangan.com', '0817 9876 026', NULL, 'On Activation', 'NEXT One', NULL, 'Sudah dikirim', '10.10.64.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(64, 'Aeon Mall BSD', 'AEON.BSD', 'OPEN', '2019-04-30', 'AEON Mall, Lantai 1, Jl. BSD Raya Utama, Pagedangan, Tangerang, Banten, 15345', 'tgr.aeon.bsd@kopikenangan.com', '0817 9876 030', NULL, 'Online', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Sudirman Park', 'SUDIRMAN.PARK', 'OPEN', '2019-04-30', 'Ruko Sudirman Park Blok B, No. 1, Lantai Dasar GF, Jl. K.H. Mas Mansyur No.Kav.35, RT.12/RW.11, Tanahabang, Jakarta Pusat, Jakarta, 10220', 'jkt.sudirman.park@kopikenangan.com', '0817 9876 035', NULL, 'On Activation', 'NEXT One', NULL, 'Sudah dikirim', '10.1.52.0/24', NULL, NULL, NULL, '172.16.0.52', NULL, NULL),
(66, 'Smartfren Plaza BII', 'SMARTFREN.BII', 'OPEN', '2019-05-06', 'Plaza BII, Food Court Area, Jl. M. H. Thamrin Kav. 22, RT.9/RW.4, Gondangdia, Menteng, Jakarta Pusat, 10230', 'jkt.smartfren.bii@kopikenangan.com', '0817 9876 037', NULL, 'On Activation', 'NEXT One', NULL, 'Sudah dikirim', '10.1.125.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(67, 'Resinda Park Mall', 'RESINDA.PARK', 'OPEN', '2019-05-09', 'Resinda Park Mall, Lantai 2, Jl. Resinda Raya No.2, Karawang Barat, Karawang, Jawa Barat, 41361', 'bks.resinda.park@kopikenangan.com', '0817 9876 041', NULL, 'On Activation', 'Telkom', '1109163684', 'Sudah dikirim', '10.10.67.0/24', '36.92.207.221/24', '36.92.207.220', '180.131.144.144/145.145', NULL, NULL, NULL),
(68, 'Pluit Village', 'PLUIT.VILL', 'OPEN', '2019-05-10', 'Pluit Village, GF Floor, Jl. Pluit Karang Ayu Barat, RT.15/RW.4, Pluit, Penjaringan, Jakarta Utara, 14450', 'jkt.pluit.vill@kopikenangan.com', '0817 9876 042', NULL, 'On Activation', 'NEXT One', NULL, 'Sudah dikirim', '10.1.53.0/24', NULL, NULL, NULL, '172.16.0.53', NULL, NULL),
(69, 'Epicentrum Walk', 'Epiwalk', 'OPEN', '2019-05-12', 'Kawasan Rasuna Epicentrum, Ground Floor, Jalan HR. Rasuna Said, RT.2/RW.5, Karet Kuningan, Setia Budi, Jakarta Selatan, Jakarta, 12940', 'jkt.epiwalk@kopikenangan.com', '0817 9876 043', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'Transmart Cempaka Putih', 'TSM.CEMPAKA', 'OPEN', '2019-05-14', 'Transmart Cempaka Putih, Ground Floor, Jl. Jend. Ahmad Yani No. 83, RT.10/RW.7, Cempaka Putih Timur, Jakarta Pusat, 10510', 'jkt.tm.cempaka@kopikenangan.com', '0817 9876 044', NULL, 'Online', 'Telkom', '926867578', 'Sudah dikirim', '10.10.70.0/24', '36.92.182.39/24', '36.92.182.38', '118.98.44.10/20', NULL, NULL, NULL),
(71, 'Mall Taman Anggrek', 'MALL.TA', 'OPEN', '2019-05-14', 'Mall Taman Anggrek, LG Floor, Jl. Letjen S. Parman No. 28, Grogol petamburan, Jakarta Barat, 11440', 'jkt.mall.ta@kopikenangan.com', '0817 9876 046', NULL, 'Online', 'Bali Fiber', '41200050140', 'Sudah dikirim', '10.10.71.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(72, 'Citywalk Gajah Mada', 'GAJAHMADA', 'OPEN', '2019-05-07', 'Citywalk Gajah Mada, Lantai Ground Floor, Jl. Gajah Mada No.19-26,Petojo Utara, Kota Jakarta Pusat, 10130', 'jkt.gajahmada@kopikenangan.com', '0817 9876 047', NULL, 'On Legal Paper', 'Telkom', NULL, NULL, '10.1.54..0/24', NULL, NULL, NULL, '172.16.0.54', NULL, NULL),
(73, 'Cinere Bellevue', 'CINERE.BELLEVUE', 'OPEN', '2019-05-15', 'Cinere Bellevue, Lantai 1, Jl. Merawan No.23, Pangkalan Jati, Cinere, Depok, Jawa Barat, 16514', 'jk.cinere.bellevue@kopikenangan.com', '0817 9876 050', NULL, 'On Activation', 'NEXT One', NULL, 'Sudah dikirim', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 'ITC Cempaka Mas', 'ITC.CEMPAKA', 'OPEN', '2019-05-10', 'ITC Cempaka Mas, Lantai LG, Jl. Cempaka Mas Timur No.586, RW.8, Sumur Batu, Kemayoran, Jakarta Pusat, 10640', 'jkt.itc.cempaka@kopikenangan.com', '0817 9876 054', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.10.74.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(75, 'Blu Plaza', 'BLU.PLAZA', 'OPEN', '2019-05-28', 'Blu Mall, Lantai Dasar, Jl. Chairil Anwar No.27-36, Margahayu, Bekasi Timur, Jawa Barat, 17113', 'jkt.blu.plaza@kopikenangan.com', '0817 9876 055', NULL, 'Online', 'Telkom', '926576748', 'Sudah dikirim', '10.10.75.0/24', '36.92.120.186/24', '36.92.120.186', '118.98.44.10/20', NULL, NULL, NULL),
(76, 'Bandara Soekarno Hatta Terminal 2D', 'SOETTA.2D', 'OPEN', '2019-05-29', 'Bandara Soekarno Hatta Terminal 2D, Benda, Tangerang, Banten. Jawa Barat 15126', 'jkt.soetta.2d@kopikenangan.com', '0817 9876 058', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.56.0/24', NULL, NULL, NULL, '172.16.0.56', NULL, NULL),
(77, 'Lippo Plaza Mampang', 'LIPPO.MAMPANG', 'OPEN', '2019-05-28', 'Lippo Plaza Mampang, Ground Floor, Jl. Warung Jati Barat No.12, RT.2/RW.5, Duren Tiga, Pancoran, Jakarta Selatan, 12760', 'jkt.lippo.mampang@kopikenangan.com', '0817 9876 063', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.55.0/24', NULL, NULL, NULL, '172.16.0.55', NULL, NULL),
(78, 'Lippo Plaza Ekalokasari Bogor', 'LIPPO.EKALOS', 'OPEN', '2019-05-29', 'Lippo Plaza Ekalokasari, Ground Floor, Jl. Siliwangi No. 123, Sukasari, Bogor, 16142', 'jkt.lippo.ekalos@kopikenangan.com', '0817 9876 065', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 'ITC Mangga Dua', 'MANGGA.DUA', 'OPEN', '2019-06-20', 'ITC Mangga Dua, Lantai Dasar, Jl. Mangga Dua Raya No.43, RT.11/RW.5, Ancol, Pademangan, Jakarta Utara - 14430', 'jkt.mangga.dua@kopikenangan.com', '0817 9876 066', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.57.0/24', NULL, NULL, NULL, '172.16.0.57', NULL, NULL),
(80, 'Plaza Indonesia', 'PLAZA.INDO', 'OPEN', '2019-06-25', 'Plaza Indonesia, Lantai 5, Jl. M.H. Thamrin No.Kav. 28-30, RT.9/RW.5, Gondangdia, Menteng, Jakarta Pusat, 10350', 'jkt.plaza.indo@kopikenangan.com', '0817 9876 068', NULL, 'Online', 'Telkom', '923830906', 'Sudah dikirim', '10.10.80.0/24', '36.92.80.223/24', '36.92.80.222', '118.98.44.10/20', NULL, NULL, NULL),
(81, 'Mega Bekasi Hypermall', 'MEGA.BKS', 'OPEN', '2019-06-26', 'Mega Bekasi Hypermall, Lantai 3, Jl. Jend. Ahmad Yani No.1, Marga Jaya, Bekasi Selatan, Bekasi, Jawa Barat, 17141', 'mega.bks@kopikenangan.com', '0817 9876 069', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.1.58.0/24', NULL, NULL, NULL, '172.16.0.58', NULL, NULL),
(82, 'Emporium Pluit', 'EMPORIUM.PL', 'OPEN', '2019-07-02', 'Emporium Pluit, Lantai 4, FC-32 Jl. Pluit Putra Kencana, RT 23/RW 8, Penjaringan, Penjaringan, Jakarta Utara - 14440', 'jkt.emporium.pl@kopikenangan.com', '0817 9876 071', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.59.0/24', NULL, NULL, NULL, '172.16.0.59', NULL, NULL),
(83, 'Living World Alam Sutera', 'LIVING.WORLD', 'OPEN', '2019-07-06', 'Living World Alam Sutera, GF, Jl. Alam Sutera Boulevard Kav. 21, 2nd floor, Pakulonan, Serpong Utara, Kota Tangerang, Banten, 15325', 'tgr.living.world@kopikenangan.com', '0817 9876 072', NULL, 'Online', 'NEXT One', NULL, 'Sudah Dikirim', '10.1.60..0/24', NULL, NULL, NULL, '172.16.0.60', NULL, NULL),
(84, 'Metropolitan Mall Bekasi', 'MM.BKS', 'OPEN', '2019-07-09', 'Metropolitan Mall, Lantai 2, Jl. KH. Noer Ali, Pekayon Jaya, Bekasi Selatan, Kota Bekasi, Jawa Barat, 17148', 'mm.bks@kopikenangan.com', '0817 9876 073', NULL, 'Online', 'NEXT One', NULL, NULL, '10.1.61.0/24', NULL, NULL, NULL, '172.16.0.61', NULL, NULL),
(85, 'Blok M Plaza', 'BLOKM.PLAZA', 'OPEN', '2019-07-10', 'Blok M Plaza, UG Floor, Jl. Bulungan No.78, RT.6/RW.6, Kramat Pela, Kebayoran Baru, Jakarta Selatan, 12130', 'jkt.blokm.plaza@kopikenangan.com', '0817 9876 074', NULL, 'Online', 'Telkom', '924045250', 'Sudah dikirim', '10.10.85.0/24', '36.92.81.95/24', '36.92.81.94', '118.98.44.10/20', NULL, NULL, NULL),
(86, 'Grand City Surabaya', 'GRANDCITY.SBY', 'OPEN', '2019-07-11', 'Grand City Surabaya, Lantai 4, Jl. Walikota Mustajab No. 1, Ketabang, Genteng, Surabaya, Jawa Timur - 60272', 'grandcity.sby@kopikenangan.com', '0817 9876 075', NULL, 'Online', 'Telkom', '924638873', 'Sudah dikirim', '10.10.86.0/24', '36.92.104.37/24', '36.92.104.36', '118.98.44.10/20', NULL, NULL, NULL),
(87, 'AEON BSD (Food Court)', 'AEON.ISLAND', 'OPEN', '2019-07-14', 'AEON BSD (Island), Food Carnival, Jl. BSD Raya Utama, Pagedangan, Tangerang, Banten, 15345', 'tgr.aeon.island@kopikenangan.com', '0817 9876 078', NULL, 'On Activation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 'Garuda HQ', 'GARUDA.HQ', 'OPEN', '2019-07-17', 'Garuda Office Cengkareng, Lantai Dasar, Jalan M1 Area Perkantoran Gedung Garuda City Center, Bandar Udara Internasional Soekarno-Hatta, Tangerang - 15111', 'tgr.garuda.hq@kopikenangan.com', '0817 9876 080', NULL, 'Online', 'Telkom', '926163097', 'Sudah dikirim', '10.10.89.0/24', '36.92.118.97/24', '36.92.118.96', '118.98.44.10/20', NULL, NULL, NULL),
(89, 'Trans Studio Mall Makassar', 'TSM.MKS', 'OPEN', '2019-07-24', 'Trans Studio Mall Makassar, LG Floor, Jl. Metro Tj. Bunga, Maccini Sombala, Tamalate, Kota Makassar, Sulawesi Selatan, 90224', 'tsm.mks@kopikenangan.com', '0817 9876 081', NULL, 'Online', 'Integrasia', NULL, 'Sudah dikirim', '10.10.92.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(90, 'Lenmarc Mall Surabaya', 'LENMARC.SBY', 'OPEN', '2019-07-25', 'Lenmarc Mall Surabaya, GF Floor, Jl. Mayjen Yono Suwoyo No.9, Pradahkalikendal, Dukuh Pakis, Surabaya, Jawa Timur, 60226', 'lenmarc.sby@kopikenangan.com', '0817 9876 085', NULL, 'Online', 'Telkom', '1114838363', 'Sudah dikirim', '10.10.90.0/24', '36.92.175.135/24', '36.92.175.134', '118.98.44.10/20', NULL, NULL, NULL),
(91, 'Phinisi Point Makassar', 'PHINISI.MKS', 'OPEN', '2019-07-26', 'Phinisi Point Makassar, LG, Jalan Metro Tanjung Bunga No.2, Panambungan, Kec. Mariso, Kota Makassar, Sulawesi Selatan 90112', 'phinisi.mks@kopikenangan.com', '0817 9876 086', NULL, 'Online', 'NEXT One', NULL, NULL, '10.1.63.0/24', NULL, NULL, NULL, '172.16.0.63', NULL, NULL),
(92, 'Mall Metro Cipulir', 'MM.CIPULIR', 'OPEN', '2019-07-26', 'Mall Metro Cipulir, GF, Jl. Ciledug Raya, RT.1/RW.1, Kebayoran lama Utara, Kebayoran lama, Jakarta Selatan, Jakarta, 12240', 'jkt.mm.cipulir@kopikenangan.com', '0817 9876 087', NULL, 'Online', 'Telkom', '924662257', 'Sudah dikirim', '10.10.96.0/24', '36.92.87.183/24', '36.92.87.182', '118.98.44.10/20', NULL, NULL, NULL),
(93, 'Gojek Kitchen Tebet', 'GO.KITCHEN', 'OPEN', '2019-08-07', 'Gojek Kitchen Tebet, Jalan Tebet Barat Dalam Raya No.64 Rt. 06/03, RT.6/RW.4, Tebet Bar., Kec. Tebet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12810', 'jkt.go.kitchen@kopikenangan.com', '0817 9876 089', NULL, 'Online', 'Moratel', NULL, 'Sudah dikirim', '10.10.95.0.24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(94, 'Malang Town Square', 'MATOS', 'OPEN', '2019-08-09', 'Malang Town Square, LG, Jl. Veteran No.2, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65111', 'mlg.matos@kopikenangan.com', '0817 9876 090', NULL, 'Online', 'Telkom', '969840519', 'Sudah dikirim', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 'Lippo Plaza Batu', 'LIPPO.BATU', 'OPEN', '2019-08-13', 'Lippo Plaza Batu, GF, Jl. Diponegoro No.1, Sisir, Kec. Batu, Kota Batu, Jawa Timur 65314', 'mlg.lippo.batu@kopikenangan.com', '0817 9876 091', NULL, 'On Activation', 'NEXT One', NULL, NULL, '10.1.64.0/24', NULL, NULL, NULL, '172.16.0.64', NULL, NULL),
(96, 'La Codefin Kemang', 'LA.CODEFIN', 'OPEN', '2019-08-15', 'La Codefin Kemang, GF, Jl. Kemang I No. 3-5, RT.11/RW.1, Bangka, Mampang Prapatan, Jakarta Selatan - 12780', 'jkt.la.codefin@kopikenangan.com', '0817 9876 096', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.10.98.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(97, 'The Vida Kebon Jeruk', 'THE.VIDA', 'OPEN', '2019-08-15', 'The Vida, GF, Jl. Perjuangan No.8 1 7, RT.1/RW.7, Kb. Jeruk, Kec. Kb. Jeruk, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11530', 'jkt.the.vida@kopikenangan.com', '0817 9876 097', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.1.65.0/24', NULL, NULL, NULL, '172.16.0.65', NULL, NULL),
(98, 'Smartfren Sabang', 'SMARTFREN.SBG', 'OPEN', '2019-08-19', 'Smartfren Sabang, Ground Floor, Jl. H. Agus Salim No.45, RT.1/RW.1, Kebon Sirih, Menteng, Jakarta Pusat, Jakarta, 10340', 'jkt.smartfren.sbg@kopikenangan.com', '0817 9876 100', NULL, 'Online', 'NEXT One', NULL, 'Sudah dikirim', '10.10.91.0/24', 'DHCP Client', NULL, NULL, NULL, NULL, NULL),
(99, 'Mall Cinere', 'CINERE.MALL', 'OPEN', '2019-08-20', 'Mall Cinere, Lantai 1, Jl. Cinere Raya, Blok Haji Rosyid No. 38, Cinere, Depok, Jawa Barat - 16514', 'jkt.cinere.mall@kopikenangan.com', '0817 9876 105', NULL, 'Online', 'Telkom', '926022608', 'Sudah dikirim', '10.10.107.0/24', '36.92.111.245/24', '36.92.111.244', '118.98.44.10/20', NULL, NULL, NULL),
(100, 'Mall Ciputra Jakarta', 'MALL.CIPUTRA', 'OPEN', '2019-08-21', 'Citraland Mall, LG, Jl. S. Parman, Tanjung Duren Utara, Grogol Petamburan, Jakarta Barat - 11470', 'jkt.mall.ciputra@kopikenangan.com', '0817 9137 981', NULL, 'Online', 'Telkom', '926228865', 'Sudah dikirim', '10.10.108.0/24', '36.92.118.125/24', '36.92.118.124', '118.98.44.10/20', NULL, NULL, NULL);
